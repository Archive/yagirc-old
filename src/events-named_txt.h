#ifndef __EVENTS_NAMED_TXT_H
#define __EVENTS_NAMED_TXT_H

#include "gui.h"
#include "intl.h"

static char *  IRCTXT_CHANMODE_CHANGE=N_("mode/%_%s%_ [%!%s%!] by %_%s\n");
static char *  IRCTXT_ERROR= N_("%_ERROR%_ %s\n");
static char *  IRCTXT_INVITE=N_("%_%s%_ invites you to %_%s\n");
static char *  IRCTXT_JOIN=N_("%_%s%_ [%!%s%!] has joined %s\n");
static char *  IRCTXT_KICK=N_("%_%s%_ was kicked from %s by %_%s%_ (%!%s%!)\n");
static char *  IRCTXT_NEW_TOPIC=N_("%_%s%_ changed the topic of %_%s%_ to: %!%s\n");
static char *  IRCTXT_NICK_CHANGED=N_("%_%s%_ is now known as %_%s\n");
static char *  IRCTXT_PART=N_("%_%s%_ [%!%s%!] has left %s (%!%s%!)\n");
static char *  IRCTXT_PONG=N_("PONG received from %s: %s\n");
static char *  IRCTXT_TOPIC_UNSET=N_("Topic unset by %_%s%_ on %_%s\n");
static char *  IRCTXT_QUIT=N_("%_%s%_ [%!%s%!] has quit IRC (%!%s%!)\n");
static char *  IRCTXT_USERMODE_CHANGE=N_("mode change [%!%s%!] for user %_%s\n");
static char *  IRCTXT_WALLOPS=N_("%s\n");
static char *  IRCTXT_YOUR_NICK_CHANGED=N_("You're now known as %_%s\n");

#endif
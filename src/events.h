#include "config.h"

#ifndef __EVENTS_H
#define __EVENTS_H

extern char *esendnick; /* nick of who sent this event */
extern char *esendaddr; /* address of who sent this event */
extern SERVER_REC *eserver; /* server which sent this event */
extern WINDOW_REC *edefwin; /* default window where to write text */

/* Initialize events */
void events_init(void);
/* Deinitialize events */
void events_deinit(void);

/* Get next parameter from data */
char *event_get_param(char **data);

/* Get next count parameters from data */
char *event_get_params(char *data, int count, ...);

#include "events-named.h"
#include "events-numeric.h"

#endif

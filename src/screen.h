#include "config.h"

#ifndef __SCREEN_H
#define __SCREEN_H

#ifdef UNIX
#  include <ncurses.h>
   typedef unsigned long SCR_CELL;
   typedef unsigned long CHAR_CELL;
#  define CHAR_BITS 24
#else
   typedef unsigned short SCR_CELL;
   typedef unsigned char CHAR_CELL;
#  define CHAR_BITS 8
#endif

#ifndef ACS_ULCORNER
#  define ACS_ULCORNER '�'
#  define ACS_URCORNER '�'
#  define ACS_LLCORNER '�'
#  define ACS_LRCORNER '�'
#  define ACS_HLINE '�'
#  define ACS_VLINE '�'
#  define ACS_CKBOARD '�'
#  define ACS_BOARD '�'
#  define ACS_LARROW ''
#  define ACS_RARROW ''
#  define ACS_UARROW ''
#  define ACS_DARROW ''
#endif

extern SCR_CELL *screen; /* Virtual screen */
extern int scrwidth, scrlen; /* Screen width and length */

int init_screen(void); /* Initialize screen, detect screen length */
void deinit_screen(void); /* Deinitialize screen */

void writestr(int x, int y, char *str, int color); /* Write string */
void writechar(int x, int y, CHAR_CELL chr, int color); /* Write character */

void clrscr(void); /* Clear screen */

void tbar(int x1, int y1, int x2, int y2, int color); /* Draw bar */
void tbox(int x1, int y1, int x2, int y2, int color); /* Draw filled box */
void shade_box(int x1, int y1, int x2, int y2, int color); /* Draw box with shadow */
void attrbar(int x1, int y1, int x2, int y2, int col); /* Change area's colors */

/* Draw box in the middle of the screen, calls also draw_screen() to draw itself. */
int middle_box(char *str, char *title, int frame_col, int text_col, int title_col);

void scroll_up(int x1, int y1, int x2, int y2); /* Scroll area up */
void scroll_down(int x1, int y1, int x2, int y2); /* Scroll area down */

/* Copy buffer to screen, should be in format
   character(char), color(char), character(char), color(char), ... */
void screencpy(void *buffer, int x1, int y1, int x2, int y2);

/* Draw text in the middle of the row */
#define wmiddle(y,str,col) writestr((scrwidth-strlen(str))/2+1,y,str,col)

/* Operating system specific, these are found from con_???.c files. */

int console_init(void); /* Initialize console, init_screen() calls this */
void console_deinit(void); /* Deinitialize console, deinit_screen() calls this */
void detect_screen_size(int *scrwidth, int *scrsize); /* Detect screen size */

void setcursor(int x, int y); /* Set cursor position */

/* Draw area to screen from virtual buffer */
void draw_screen(int x1, int y1, int x2, int y2);

extern CHAR_CELL acs[6]; /* ACS_* characters, for @ACS_MACROS@ */

#endif

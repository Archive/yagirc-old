#include "config.h"

#ifndef __GUI_MENUFACTORY_H
#define __GUI_MENUFACTORY_H

#include "data.h"

void gui_menu_create(WINDOW_REC *win);
void gui_menu_destroy(WINDOW_REC *win);

void menus_set_sensitive(WINDOW_REC *win, char *path, int sensitive);

#endif

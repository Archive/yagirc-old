#include "config.h"

#ifndef __DCC_H
#define __DCC_H

#include "data.h"

/* Initialize DCC */
void dcc_init(void);
/* Deinitialize DCC */
void dcc_deinit(void);


/* /DCC CHAT command */
int dcc_chat(char *data);
/* /DCC SEND command */
int dcc_send(char *data);
/* /DCC GET command */
int dcc_get(char *data);
/* /DCC LIST command */
int dcc_list(char *data);
/* /DCC CLOSE command */
int dcc_close(char *data);

/* Abort DCC transfer */
int dcc_abort(DCC_REC *dcc);

/* Handle DCC CTCP commands */
int dcc_handle_ctcp(char *sender, char *data);
/* Handle DCC replies */
int dcc_reply(char *sender, char *data);

/* Find DCC record, arg can be NULL */
DCC_REC *dcc_find_item(int type, char *nick, char *arg);
/* Send text to DCC chat */
int dcc_chat_write(DCC_REC *dcc, char *str);
/* Close specified DCC record */
int dcc_find_close(char *nick, int type, char *fname);

extern GList *dcclist;

#endif

#include "config.h"

#ifndef __GUI_SETUP_ALIAS_H
#define __GUI_SETUP_ALIAS_H

ALIAS_REC *add_alias_list(char *alias, char *cmd);
void remove_alias_list(ALIAS_REC *rec);
void gui_setup_aliases(GtkWidget *vbox);

#endif

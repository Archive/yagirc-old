#include "config.h"

#include <gtk/gtk.h>
#include <glib.h>


#include "intl.h"
#include "irc.h"
#include "gui.h"
#include "gui_setup_settings.h"

#include "data.h"
#include "glib.h"
#include "misc.h"

static GtkWidget *sound, *soundpath, *acceptdccchat, *acceptdccsend, *autoaskondcc, *autoquery, *autowin_label, *autowin_box, *autowin_chan, *autowin_query;
static GtkWidget *invmode, *logtofile, *logpath, *hbox, *frame, *radio_but1, *radio_but2, *radio_but3, *radio_but4, *urlcatch, *browserpath;
static GSList *group;

void gui_setup_guisettings_accept(void)
{
GList *tmp;
int toolb;

  global_settings->autoaskondcc = GTK_TOGGLE_BUTTON(autoaskondcc)->active;
  global_settings->autoquery = GTK_TOGGLE_BUTTON(autoquery)->active;
  global_settings->autowin_chan = GTK_TOGGLE_BUTTON(autowin_chan)->active;
  global_settings->autowin_query = GTK_TOGGLE_BUTTON(autowin_query)->active;

	if (GTK_TOGGLE_BUTTON(radio_but1)->active) {
		global_settings->toolbar_style = 0;
		toolb = GTK_TOOLBAR_ICONS; 
 	}
		else if (GTK_TOGGLE_BUTTON(radio_but2)->active) {
		global_settings->toolbar_style = 1;
		toolb = GTK_TOOLBAR_TEXT;
	}
		else if (GTK_TOGGLE_BUTTON(radio_but3)->active) {
		global_settings->toolbar_style = 2;
		toolb = GTK_TOOLBAR_BOTH;
	}
	else {
		global_settings->toolbar_style = 3;
	}
         
	GLIST_FOREACH(tmp, winlist) {
		WINDOW_REC *win;
	
		win = (WINDOW_REC *) tmp->data;

		if (global_settings->toolbar_style != 3) {
			gtk_toolbar_set_style(GTK_TOOLBAR(win->gui->parent->toolbar), toolb);
			gtk_widget_show (win->gui->parent->toolbar);
		}
		else {
			gtk_widget_hide (win->gui->parent->toolbar);
		}
	}
}

void gui_setup_othersettings_accept(void)
{
  global_settings->acceptdccchat = GTK_TOGGLE_BUTTON(acceptdccchat)->active;
  global_settings->acceptdccsend = GTK_TOGGLE_BUTTON(acceptdccsend)->active;
  global_settings->urlcatch = GTK_TOGGLE_BUTTON(urlcatch)->active;
  gui_get_label(global_settings->browserpath, browserpath);
  global_settings->sound = GTK_TOGGLE_BUTTON(sound)->active;
  global_settings->invmode = GTK_TOGGLE_BUTTON(invmode)->active;
  global_settings->logtofile = GTK_TOGGLE_BUTTON(logtofile)->active;
  gui_get_label(global_settings->logpath ,logpath);
  gui_get_label(global_settings->soundpath, soundpath);
}

void gui_setup_guisettings(GtkWidget *vbox)
{
  autoaskondcc = gtk_check_button_new_with_label("Auto Popup Ask About DCC");
  autoquery = gtk_check_button_new_with_label("Auto Query on Msg");
  autowin_label = gtk_label_new("Auto Create New Window On:");
  autowin_box = gtk_hbox_new(TRUE, 10);
  autowin_chan = gtk_check_button_new_with_label("On Channel Join");
  autowin_query = gtk_check_button_new_with_label("On Query");

  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(autowin_query), global_settings->autowin_query);
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(autowin_chan), global_settings->autowin_chan);
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(autoquery), global_settings->autoquery);
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(autoaskondcc), global_settings->autoaskondcc);


/* Toolbar settings selection */

  frame = gtk_frame_new("Toolbar visual:");
  hbox = gtk_hbox_new (TRUE, 0);
  
  gtk_container_add(GTK_CONTAINER(frame), hbox);
  
  gtk_widget_show (hbox);
  gtk_widget_show (frame);
  
  radio_but1 = gtk_radio_button_new_with_label (NULL, "Icon");
  gtk_box_pack_start (GTK_BOX (hbox), radio_but1, TRUE, TRUE, 0);
  gtk_widget_show (radio_but1);

  group = gtk_radio_button_group (GTK_RADIO_BUTTON (radio_but1));

  radio_but2 = gtk_radio_button_new_with_label (group, "Text");
  gtk_box_pack_start (GTK_BOX (hbox), radio_but2, TRUE, TRUE, 0);
  gtk_widget_show (radio_but2);

  group = gtk_radio_button_group (GTK_RADIO_BUTTON (radio_but2));

  radio_but3 = gtk_radio_button_new_with_label (group, "Both");
  gtk_box_pack_start (GTK_BOX (hbox), radio_but3, TRUE, TRUE, 0);
  gtk_widget_show (radio_but3);

  group = gtk_radio_button_group (GTK_RADIO_BUTTON (radio_but3));

  radio_but4 = gtk_radio_button_new_with_label (group, "disable");
  gtk_box_pack_start (GTK_BOX (hbox), radio_but4, TRUE, TRUE, 0);
  gtk_widget_show (radio_but4);


  gtk_box_pack_start( GTK_BOX(vbox), frame, FALSE, TRUE, 4); /* 4 ?? */

 switch (global_settings->toolbar_style) {
 case 0:
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(radio_but1), TRUE);
	break;
 case 1:
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(radio_but2), TRUE);
	break;
 case 2:
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(radio_but3), TRUE);
	break;
 case 3:
 	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(radio_but4), TRUE);
 
 }
 
  gtk_box_pack_start( GTK_BOX(vbox), autoaskondcc, FALSE, FALSE, 1);
  gtk_box_pack_start( GTK_BOX(vbox), autoquery, FALSE, FALSE, 1);
  gtk_box_pack_start( GTK_BOX(vbox), autowin_label, FALSE, FALSE, 1);
  gtk_box_pack_start( GTK_BOX(vbox), autowin_box, FALSE, FALSE, 1);
  gtk_box_pack_start( GTK_BOX(autowin_box), autowin_chan, FALSE, FALSE, 11);
  gtk_box_pack_start( GTK_BOX(autowin_box), autowin_query, FALSE, FALSE, 11);

  gtk_widget_show(autoaskondcc);
  gtk_widget_show(autoquery);
  gtk_widget_show(autowin_label);
  gtk_widget_show(autowin_box);
  gtk_widget_show(autowin_chan);
  gtk_widget_show(autowin_query);
}

void gui_setup_othersettings(GtkWidget *vbox)
{
  acceptdccchat = gtk_check_button_new_with_label("Accept DCC Chats");
  acceptdccsend = gtk_check_button_new_with_label("Accept DCC Sends");
  sound = gtk_check_button_new_with_label("Sound");
  invmode = gtk_check_button_new_with_label("Invisible mode");
  logtofile = gtk_check_button_new_with_label("Log to files (one log file per channel) ");
  urlcatch = gtk_check_button_new_with_label("URL Catcher");

  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(invmode), global_settings->invmode);
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(logtofile), global_settings->logtofile);
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(sound), global_settings->sound);
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(acceptdccsend), global_settings->acceptdccsend);
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(acceptdccchat), global_settings->acceptdccchat);
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(urlcatch), global_settings->urlcatch);

  gtk_box_pack_start( GTK_BOX(vbox), acceptdccchat, FALSE, FALSE, 1);
  gtk_box_pack_start( GTK_BOX(vbox), acceptdccsend, FALSE, FALSE, 1);
  gtk_box_pack_start( GTK_BOX(vbox), invmode, FALSE, FALSE, 1);

  gtk_box_pack_start( GTK_BOX(vbox), sound, FALSE, FALSE, 1);
  soundpath = gui_add_label(vbox, ("Path:"), global_settings->soundpath);

  gtk_box_pack_start( GTK_BOX(vbox), logtofile, FALSE, FALSE, 1);
  logpath = gui_add_label(vbox, ("Path:"), global_settings->logpath);

  gtk_box_pack_start( GTK_BOX(vbox), urlcatch, FALSE, FALSE, 1);
  browserpath = gui_add_label(vbox, ("Browser:"), global_settings->browserpath);

  gtk_widget_show(acceptdccchat);
  gtk_widget_show(acceptdccsend);
  gtk_widget_show(sound);
  gtk_widget_show(soundpath);
  gtk_widget_show(invmode);
  gtk_widget_show(logtofile);
  gtk_widget_show(logpath);
  gtk_widget_show(urlcatch);
  gtk_widget_show(browserpath);
}

#
# The yagIRC script prototype + short manual
#
#  Copyright (C) 1998 Jere Sanisalo <jeress@iname.com> and Timo Sirainen
# 

# CUSTOM SCRIPTS

# use startup.pm module - new scripts should always be made as modules and
# placed here to load them.
use startup;

# Commands (alphabetic order):
#
# NOTES:
#  - !! means command doesn't work yet.
#  - Return values are still a bit unfinished, some commands return nothing
#    in error, some return -1 and some return undef..
#  - documentation is still a bit out of date..
#
#
#  ircBind(eventType,eventSpecifier,bindTime,functionName[,serverId]);
#         Binds an event/command to a SUB-function. The function depends on
#         the event. The bindTime is the amount of time (in milliseconds) the
#         binding lasts (0 = forever).
#
#         To bind a command, give it in eventType, WITH the slash-sign ('/').
#         For example, to bind the QUIT-command, call the ircBind like this:
#           ircBind("/QUIT","",0,"myCustomFunction");
#         A list of command-specific eventSpecifiers is found after the event
#         list.
#
#         The callback functions should return 'no' if they want the yagIRC
#         to process the event also. Note that if many functions have been
#         assigned to an event, then everyone must return 'no' for yagIRC to
#         process the event. If nothing is returned, then the return value
#         defaults to 'no'.
#
#         List of all events (non-commands):
#          ACTION   - ...
#                     The function is in the type of:
#                       functionName(nickName,userHost,dest,message);
#          CONNECTED - Called when the server connection has been established
#                      (and after the user has been logged on).
#                      The function is in the type of:
#                        functionName(serverId,serverName);
#          CTCP     - Called when someone sends a CTCP-request (eg. a
#                     'VERSION').
#                     The function is in the type of:
#                       functionName(nickName,userHost,dest,ctcpData);
#          DCCMSG   - Called when a dcc message has been received. The
#                     eventSpecifier notifies the user nick on which to bind
#                     the event (the connection may not necessary be up)..
#                     The function is in the type of:
#                       functionName(nickName,userHost,message);
#          DISCONNECTED - Called when the server connection has been
#                         disconnected.
#                         The function is in the type of:
#                           functionName(serverId,serverName);
#          FLOOD    - Called when someone floods you or some channel.
#                     dest is channel name or your nick. level is the type of
#                     flood
#                         The function is in the type of:
#                           functionName(nickName,userHost,dest,level);
#          INVITE   - Called when someone invites you to a channel.
#                     The function is in the type of:
#                       functionName(nickName,userHost,channelName);
# !!       IRCJOIN  - Called when someone joins the irc (kind of a notify).
#                     The irc-nick is given in eventSpecifier.
#                     The function is in the type of:
#                       functionName(nickName,userHost);
# !!       IRCPART  - Called when someone leaves the irc (kind of a notify).
#                     The irc-nick is given in eventSpecifier.
#                     The function is in the type of:
#                       functionName(nickName,userHost);
#          JOIN     - Called when someone joins a channel. The eventSpecifier
#                     notifies the channel on which to bind the event (""
#                     means all channels).
#                     The function is in the type of:
#                       functionName(nickName,userHost,channelName);
#                     NOTE! Here only the channelName has the server info.
#          KICK     - Called when someone is kicked out of a channel you're
#                     on.
#                     The function is in the type of:
#                       functionName(kickerNick,ChannelName,kickedNick,kickedUserHost,kickReason);
#                     NOTE! Here only the channelName has the server info.
#          MODE     - Called when the channel mode has been changed. The
#                     eventSpecifier notifies the channel on which to bind
#                     the event ("" means all channels).
#                     The function is in the type of:
#                       functionName(nickName,channelName,modeString);
#                     NOTE! The modeString contains the whole mode-string,
#                           ie. '+oo-o john ira turbo'.
#          PART     - Called when someone leaves a channel you're on. The
#                     eventSpecifier notifies the channel on which to bind
#                     the event ("" means all channels).
#                     The function is in the type of:
#                       functionName(nickName,userHost,channelName,leaveMsg);
#          PRGQUIT  - Called upon the program quit (or when script is
#                     unloaded).
#                     The function is in the type of:
#                       functionName();
#          PRIVMSG  - Called when a private message is written to you.
#                     The function is in the type of:
#                       functionName(nickName,userHost,message);
#          PUBMSG   - Called when a message is written in a channel. The
#                     eventSpecifier notifies the channel on which to bind
#                     the event ("" means all channels).
#                     The function is in the type of:
#                       functionName(nickName,channelName,message);
#                     NOTE! Here only the channelName has the server info.
#          SERVERMSG - Called when the server sends up some text (ie. motd or
#                      anything, just as long as it's sent by the server).
#                      The function is in the type of:
#                        functionName(serverId,message);
#          TOPIC    - Called when the topic changes on a channel you're on.
#                     The function is in the type of:
#                       functionName(nickName,userHost,channelName,topic);
#          QUIT     - Called when someone who was on one or more of the 
#                     channels you're on quits.
#                     The function is in the type of:
#                       functionName(nickName,userHost,channelNames,leaveMsg);
#                     NOTE! The channelNames is a list of all the channels
#                           you know the user was before quitting, separated
#                           by a space.
#                     NOTE! This is called before IRCPART.
#
#         FOR COMMANDS ONLY:
#         The eventSpecifier depends on eventType. It's usually channel or
#         nick name, here's the list of special eventTypes:
#
#          CONNECTED, DISCONNECTED, SERVERMSG:
#             The eventSpecifier specifies the server ID to bind.
#
#          QUIT:
#             eventSpecifier doesn't do anything...
#
#         You can give "" to any of these, which uses the default (and the
#         the defaults usually tries to bind it to everything).
#
#         RETURN VALUES(S): The event binding id (given by yagIrc), or "" if
#                           there was an error.
#         NOTE! Use ircGetEventServer() to find out where the event came from
#               in functions.
#         NOTE! Beware of looped commands (bind a command and inside that
#               command call the same command again (infinite recursive
#               loop)).
#  ircCmd(command[,channelName[,serverId]]);
#         Issues a command (like "names #test").
#         RETURN VALUES(S): 'yes' or 'no' depending on success.
#         NOTE! Beware of looped commands (bind a command and inside that
#               command call the same command again (infinite recursive
#               loop)).
#  ircCTCPSend(dest,ctcpData[,serverId]);
#         Sends a CTCP request to a user (dest can be a channel or a nick).
#         RETURN VALUES(S): 'yes' or 'no' depending on success.
#  ircCTCPReply(nickName,ctcpData[,serverId]);
#         Sends a CTCP reply to a user.
#         RETURN VALUES(S): 'yes' or 'no' depending on success.
#  ircGetChannelList([serverId]);
#         Gets the list of joined channels in a server.
#         RETURN VALUES(S): Channel list (list of strings).
#  ircGetClientInfo([infoId[, infoId[, ...]]]);
#         Returns a yagIrc client info-field (or all fields if the infoId is
#         omitted) in a strings.
#         Here's a list of currently supported ids:
#          UPTIME   - Returns the uptime of yagIrc (not the OS).
#          VER      - Returns the complete version information.
#          VERONLY  - Returns the version of yagIrc (only the version, in the
#                     form of 'vX.XX' or something like that).
#         NOTE! When retrieving all fields, the infos will be returned in
#               pairs (IDKEY, VALUE) and the pairs are not ordered in any way.
#  ircGetEventServer();
#         Returns the server the event came from.
#  ircGetCurrentServer();
#         Returns the current (active) server id. The active server is changed
#         for the duration of certain events, so this command can be used to
#         fetch the server where the action is.
#         RETURN VALUES(S): The current active server id. If not connected to
#                           any server, -1 is returned.
#  ircGetLocalUserInfo([serverId]);
#         Gets the local user (the user the yagIrc is connected) info on a
#         server.
#         RETURN VALUES(S): On failure a 'undef' is returned.
#                           On success, the return is a list, which has the
#                           the folloring items (in the same order):
#                             userNick,userHost,userName
#  ircGetNameList(channelName[,serverId]);
#         Gets the users of a channel. The list returned will contain a
#         string per user (the user will have any irc-additions in from of
#         the nick, ie. a '@' if the user is an operator and a '+' is the
#         user has voices etc.).
#         RETURN VALUES(S): User list (list of strings).
#  ircGetServerIdList();
#         Gets a list of the connected server ids and the servers addresses.
#         The list has ID-ADDRESS-pairs, so the return value is ready to be
#         hashed.
#         RETURN VALUES(S): Server Id+address list (list of strings).
#  ircGetTopic(channelName[,serverId]);
#         Gets the channel topic.
#         RETURN VALUES(S): The topic is returned, or 'undef' if there was an
#                           error.
#  ircMenuAdd(menuType,menuText,menuDest,functionName[,serverId]);
#         Adds a menu item to any of the menus (main/channel/nick). The
#         menuText is the text displayed in the menu. The menuDest is the
#         destination for the menu item (if the menuType does not have a
#         destination, then set this to "", see the list at the end of the
#         the command-description). The functionName is the callback function
#         (defined as: functionName(menuType,menuDest,serverId,menuItemId);).
#         The menu types are as follow:
#           GLOBAL - Global menus for every window (no menuDest!).
#           CHANNEL - Channel specific pop-up menus (menuDest is the channel
#                     name).
#           NICK - Nick name specific pop-up menus (menuDest is the nick name
#                  to bind).
#         RETURN VALUES(S): The menuItemId (for later menu access). On error
#                           the returned value is "".
#         NOTE! One type of menu binding can only be done once!
#  ircMenuRemove(menuItemId);
#         Removes the menu item by menuItemId.
#         RETURN VALUES(S): 'yes' or 'no' depending on success.
#  ircMenuRemoveMatch(menuType,menuText,menuDest,serverId,functionName);
#         Removes the menu item by the exact menu definition.
#         RETURN VALUES(S): 'yes' or 'no' depending on success.
#  ircMsg(dest,message[,serverId]);
#         Sends a /msg, dest can be #channel name, nick name or =dcc nick
#         name.
#         RETURN VALUES(S): 'yes' or 'no' depending on success.
#  ircNotice(dest,message[,serverId]);
#         Sends a /notice to a user/channel/dcc.
#         RETURN VALUES(S): 'yes' or 'no' depending on success.
#  ircServerCmd(command[,serverId]);
#         Sends raw, unformatted data to the server.
#         RETURN VALUES(S): 'yes' or 'no' depending on success (the actual
#                           command might fail!).
#  ircText(text[,channelName[,serverId]]);
#         Writes text to screen (in specific channel).
#         RETURN VALUES(S): 'yes' or 'no' depending on success.
#         NOTE! If channelName is "", then the text will go to the current
#               active window.
#  ircTimerCallback(functionName,timeMS,...);
#         Hooks up a callback timer. After the specified amount of time
#         (given in milliseconds) the functionName is called with the
#         specified parameters (timeMS + everything after it).
#         The callback function must return 'yes' or 'no', depending on
#         whether the callback should stop here. So returning 'yes' keeps the
#         timer calling itself (with the same parameters) and returning 'no'
#         removes this timer from the timer-stack.
#         RETURN VALUES(S): The timerId (for later timer deletion). On error
#                           the returned value is -1.
#         NOTE! If you hook up any more timer callbacks in a timer callback
#               function, just be sure the hooks end up somewhere (so you
#               don't accidentally hook up over a million timer callbacks,
#               which is, without saying, slow ;).
#         NOTE! It's not necessary to store the timerId anywhere if you don't
#               plan to terminate the timer from somewhere else than the timer
#               callback-function.
#  ircTimerRemove(timerId)
#         Removes a previously hooked timer by the timerId returned by
#         ircTimerCallback.
#         RETURN VALUES(S): 'yes' or 'no' depending on success.
#  ircUnBind(bindId)
#         Unbinds a previously bound event by the bindId returned by ircBind.
#         RETURN VALUES(S): 'yes' or 'no' depending on success.
#  ircUnBindMatch(eventType,eventSpecifier,functionName[,serverId]);
#         Unbinds a previously bound event (everything must be a perfect
#         match). If you happen to have multiple bindings with exactly the
#         same parameters, then all of 'em will be unbound.
#         RETURN VALUES(S): 'yes' or 'no' depending on success.
#


#  HOW YAGIRC SCRIPTS MANAGES MULTIPLE SERVERS
#
#YagIrc can be connected to multiple servers at the same time. This, in the
#view of scripts, presents few problems. Here's how they are solved in yagIRC.
#Each server is given a serverId by yagIRC. This serverId presents the server
#from there on. You send and receive all information to yagIRC using these
#serverIds. When the serverId is not one of the parameters, the current server
#id is used (ircGetCurrentServer() can be used to fetch it). On most cases,
#if the server name is not given, then the current active server is used.


#  NOTES ON BOUND COMMANDS
#
#The script can bind any command to it's need, ie. the '/MSG' or '/QUIT'
#commands. This means that the original command is out of reach. There may
#be some need to still access the old commands, so by putting '//MSG' or
#'//QUIT' ignores effectively any bindings and ALWAYS uses the unbound
#commands (these work in the yagIRC itself also).


#  NOTES ON USERHOST
#
#The userHost returned by some events/commands is in the form of
#'who@where', not the common 'nick!who@where'. The nick is always given in a
#separate statement (to save a line or two of perl code required to split it).

ircText("My great IRC script loaded.\n");

/*

 net_nowait.c : Threading for net_connect()

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"
#include <stdio.h>
#include <glib.h>

#include "network.h"
#include "net_nowait.h"

#ifdef USE_PTHREADS

#include <pthread.h>

typedef struct
{
    char *server;
    int port;
    NET_CALLBACK func;
    void *data;
}
NET_THREAD_REC;

static void *net_callback(void *data)
{
    NET_THREAD_REC *rec;
    int ret;

    rec = (NET_THREAD_REC *) data;
    ret = net_connect(rec->server, rec->port);

    rec->func(ret, rec->data);

    g_free(rec->server);
    g_free(rec);

    return NULL;
}

int net_nowait_connect(char *server, int port, NET_CALLBACK func, void *data)
{
    NET_THREAD_REC *rec;
    pthread_t thread;

    g_return_val_if_fail(server != NULL, 0);
    g_return_val_if_fail(func != NULL, 0);

    rec = g_new(NET_THREAD_REC, 1);
    rec->server = g_strdup(server);
    rec->port = port;
    rec->func = func;
    rec->data = data;

    if (pthread_create (&thread, NULL, net_callback, rec))
    {
        g_warning("net_nowait_connect() : pthread_create() failed!");
        func(net_connect(server, port), data);
        return 0;
    }
    return 1;
}
#else
int net_nowait_connect(char *server, int port, NET_CALLBACK func, void *data)
{
    g_return_val_if_fail(server != NULL, 0);
    g_return_val_if_fail(func != NULL, 0);

    func(net_connect(server, port), data);
    return 1;
}
#endif

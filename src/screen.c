/* SkyReader v2.00 - Copyright (c) 1997 Timo Sirainen */

/* screen.c : All virtual screen management, real screen management is in
              con_???.c files. */

#include "config.h"

#ifdef USE_TEXTUI
#include <stdio.h>
#include <string.h>

#include <glib.h>

#include "screen.h"

SCR_CELL *screen; /* Virtual screen */
int scrwidth, scrlen; /* Screen width and length */

CHAR_CELL acs[6]; /* ACS_* characters, for @ACS_MACROS@ */

/* Initialize acs[] array */
static void init_acs_chars(void)
{
    acs[0] = ACS_CKBOARD;
    acs[1] = ACS_BOARD;
    acs[2] = ACS_LARROW;
    acs[3] = ACS_RARROW;
    acs[4] = ACS_UARROW;
    acs[5] = ACS_DARROW;
}

/* Initialize screen, detect screen length */
int init_screen(void)
{
    if (!console_init()) return 0;

    detect_screen_size(&scrwidth, &scrlen);

    screen = g_new0(SCR_CELL, scrwidth*scrlen);

    init_acs_chars();
    return screen != NULL;
}

/* Deinitialize screen */
void deinit_screen(void)
{
    if (screen != NULL)
    {
        g_free(screen);
        screen = NULL;
    }

    console_deinit();
}

/* Write string */
void writestr(int x, int y, char *str, int color)
{
    SCR_CELL *scr;

    x--; y--;
    if (y >= scrlen) y = scrlen-1;

    scr = screen + x+y*scrwidth;
    for (; *str != '\0'; str++, scr++)
        *scr = (SCR_CELL) ((unsigned char) *str + ((SCR_CELL) color << CHAR_BITS));
}

/* Write character */
void writechar(int x, int y, CHAR_CELL chr, int color)
{
    if (y > scrlen) y = scrlen;
    screen[(x-1) + (y-1)*scrwidth] = (SCR_CELL) (chr + ((SCR_CELL) color << CHAR_BITS));
}

/* Clear screen */
void clrscr(void)
{
    memset(screen, 0, scrwidth*scrlen*sizeof(screen[0]));
}

/* Draw bar */
void tbar(int x1, int y1, int x2, int y2, int color)
{
    SCR_CELL *scr;
    int num, num2;

    x1--; y1--; x2--; y2--;

    x2 -= x1;
    for (num2 = y1; num2 <= y2; num2++)
    {
        scr = screen + x1+num2*scrwidth;
        for (num = x2; num >= 0; num--, scr++)
            *scr = (SCR_CELL) (' ' + ((SCR_CELL) color << CHAR_BITS));
    }
}

/* Draw filled box */
void tbox(int x1, int y1, int x2, int y2, int color)
{
    int num, num2;

    /* Draw box manually */
    writechar(x1, y1, ACS_ULCORNER, color);
    writechar(x2, y1, ACS_URCORNER, color);
    writechar(x1, y2, ACS_LLCORNER, color);
    writechar(x2, y2, ACS_LRCORNER, color);
    for (num = 1; num < x2-x1; num++)
    {
        writechar(x1+num, y1, ACS_HLINE, color);
        writechar(x1+num, y2, ACS_HLINE, color);
    }
    for (num = y1+1; num < y2; num++)
    {
        writechar(x1, num, ACS_VLINE, color);
        writechar(x2, num, ACS_VLINE, color);
    }
    for (num2 = y1; num2 < y2-1; num2++)
        for (num = x1; num < x2-1; num++)
            screen[num + num2*scrwidth] = (SCR_CELL) (' ' + ((SCR_CELL) color << CHAR_BITS));
}

/* Draw box with shadow */
void shade_box(int x1, int y1, int x2, int y2, int color)
{
    tbox(x1, y1, x2, y2, color);
    attrbar(x1+2, y2+1, x2+2, y2+1, 8);
    attrbar(x2+1, y1+1, x2+2, y2, 8);
}

/* Change area's colors */
void attrbar(int x1, int y1, int x2, int y2, int col)
{
    char *addr;
    int x,y,diff;

    if (x2 > scrwidth) x2 = scrwidth;

    addr = (char *) screen + (x1-1)*sizeof(screen[0])+(CHAR_BITS/8) + (y1-1)*scrwidth*sizeof(screen[0]);
    diff = (scrwidth-x2 + x1-1)*sizeof(screen[0]);
    x2 -= x1-1; y2 -= y1-1;
    for (y=y2; y > 0; y--)
    {
        for (x = x2; x > 0; x--)
        {
            *addr = (char) col;
            addr += sizeof(screen[0]);
        }
        addr += diff;
    }
}

/* Draw box in the middle of the screen, calls also draw_screen() to draw itself. */
int middle_box(char *str, char *title, int frame_col, int text_col, int title_col)
{
    register int pos, len = strlen(str);
    char tmp[256];

    pos = (scrwidth-len)/2-1;
    tbox(pos, scrlen/2-2, pos+len+3, scrlen/2+2, frame_col);
    wmiddle(scrlen/2, str, text_col);
    if (title != NULL)
    {
        sprintf(tmp, " %s ", title);
        wmiddle(scrlen/2-2, tmp, title_col);
    }

    draw_screen(pos, scrlen/2-2, pos+len+3, scrlen/2+2);
    return pos+2;
}

/* Scroll area up */
void scroll_up(int x1, int y1, int x2, int y2)
{
    int y;

    x2 = (x2-x1+1)*sizeof(screen[0]); x1--;
    for (y=y1-1; y<=y2-2; y++)
        memcpy(&screen[x1+y*scrwidth], &screen[x1+(y+1)*scrwidth], x2);
}

/* Scroll area down */
void scroll_down(int x1, int y1, int x2, int y2)
{
    int y;

    x2 = (x2-x1+1)*sizeof(screen[0]); x1--;
    for (y=y2-1; y>=y1; y--)
        memcpy(&screen[x1+y*scrwidth], &screen[x1+(y-1)*scrwidth], x2);
}

/* Copy buffer to screen, should be in format
   character(char), color(char), character(char), color(char), ... */
void screencpy(void *buffer, int x1, int y1, int x2, int y2)
{
    unsigned short *cell;
    SCR_CELL *scr;

    x1--; y1--; y2--;

    cell = (unsigned short *) buffer;
    for (; y1 <= y2; y1++)
    {
        scr = screen + (x1+y1*scrwidth);
#if CHAR_BITS == 8
        memcpy(scr, cell, x2-x1); cell += x2-x1;
#else
        {
            int x;
            for (x = x1; x < x2; x++, cell++, scr++)
                *scr = (*cell & 0xff) + ((*cell & 0xff00) << (CHAR_BITS-8));
        }
#endif
    }
}
#endif

/*

 log.c : Logging functions

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"
#include <stdio.h>
#include <string.h>

#include <glib.h>

#include <unistd.h>
#include <fcntl.h>

#include "os.h"
#include "irc.h"
#include "misc.h"
#include "intl.h"

/* Append/create to log file, data = logging info */
LOG_REC *log_file_open(char *fname, char *data)
{
    LOG_REC *rec;
    char *ptr;
    int handle, *level;
    time_t t;

    handle = open(fname, O_WRONLY | O_APPEND | O_CREAT, FILE_CREATE_MODE);
    if (handle == -1) return NULL;
    lseek(handle, 0, SEEK_END);

    ptr = _("--- Log opened at ");
    write(handle, ptr, strlen(ptr));

    t = time(NULL);
    ptr = asctime(localtime(&t));
    write(handle, ptr, strlen(ptr));

    rec = g_new0(LOG_REC, 1);
    rec->fname = g_strdup(fname);
    rec->handle = handle;

    g_strup(data);

    rec->level = LEVEL_ALL; level = &rec->level;
    for (ptr = data; ; data++)
    {
        if (*data == ' ' || *data == '\0')
        {
            int neg, n;

            if (*data == ' ') *data++ = '\0';

            if (*ptr == '-' || *ptr ==  '+')
            {
                /* window level */
                neg = *ptr == '-' ? 1 : 0;
                ptr++;

                if (strcmp(ptr, "ALL") == 0)
                {
                    if (!neg)
                        *level |= LEVEL_ALL;
                    else
                        *level &= ~LEVEL_ALL;
                }
                else
                {
                    for (n = 0; n < LEVELS; n++)
                    {
                        if (strcmp(ptr, levels[n]) == 0)
                        {
                            if (!neg)
                                *level |= 1 << n;
                            else
                                *level &= ~(1 << n);
                            break;
                        }
                    }
                }
            }
            else
            {
                /* channel/nick name */
                LOG_ITEM_REC *item;

                item = g_new(LOG_ITEM_REC, 1);
                item->name = g_strdup(ptr);
                item->level = rec->level; level = &item->level;

                rec->items = g_list_append(rec->items, item);
            }

            while (*data == ' ') data++;
            if (*data == '\0') break;
            ptr = data;
        }
    }

    logs = g_list_append(logs, rec);

    return rec;
}

/* Close log file */
void log_file_close(LOG_REC *rec)
{
    char *ptr;
    time_t t;

    ptr = _("--- Log closed at ");
    write(rec->handle, ptr, strlen(ptr));

    t = time(NULL);
    ptr = asctime(localtime(&t));
    write(rec->handle, ptr, strlen(ptr));

    logs = g_list_remove(logs, rec);

    g_free(rec->fname);
    close(rec->handle);
    g_free(rec);
}

/* Write to log file */
void log_file_write(char *chan, int level, char *data)
{
    GList *tmp;
    time_t t;
    struct tm *tm;
    char str[10];

    t = time(NULL);
    tm = localtime(&t);
    sprintf(str, "%02d:%02d ", tm->tm_hour, tm->tm_min);

    GLIST_FOREACH(tmp, logs)
    {
        LOG_REC *rec;
        GList *sub;
        int ok;

        rec = (LOG_REC *) tmp->data;

        ok = (level & rec->level) != 0;
        GLIST_FOREACH(sub, rec->items)
        {
            LOG_ITEM_REC *rec;

            rec = (LOG_ITEM_REC *) sub->data;
            if (chan != NULL && strcasecmp(rec->name, chan) == 0)
                ok = (level & rec->level) != 0;
        }

        if (ok)
        {
            /* write to log.. */
            write(rec->handle, str, strlen(str));
            write(rec->handle, data, strlen(data));
            write(rec->handle, "\n", 1);
        }
    }
}

/* Find a log file record */
LOG_REC *log_file_find(char *fname)
{
    GList *tmp;

    GLIST_FOREACH(tmp, logs)
    {
        LOG_REC *rec;

        rec = (LOG_REC *) tmp->data;
        if (strcmp(rec->fname, fname) == 0) return rec;
    }
    return NULL;
}


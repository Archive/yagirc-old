#ifndef __GUI_TXT_H
#define __GUI_TXT_H

#include "gui.h"
#include "intl.h"

static char *  IRCTXT_IRCJOIN=N_("%_%s%_ has joined to IRC\n");
static char *  IRCTXT_IRCPART=N_("%_%s%_ has left IRC\n");

#endif

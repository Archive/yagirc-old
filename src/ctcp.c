/*

 ctcp.c : Functions handling CTCP events

    Copyright (C) 1998 Timo Sirainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <glib.h>

#include "os.h"
#include "ctcp_txt.h"
#include "dcc.h"
#include "irc.h"
#include "version.h"
#include "events.h"
#include "options.h"
#include "sound.h"

/* CTCP reply sending function */
int ctcp_timeout_func(SERVER_REC *server)
{
    g_return_val_if_fail(server != NULL, 1);

    if (server->ctcpqueue == NULL) return 1;

    server->ctcpqueue = g_list_first(server->ctcpqueue);
    irc_send_cmd(server, (char *) server->ctcpqueue->data);
    g_free(server->ctcpqueue->data);

    server->ctcpqueue = g_list_remove_link(server->ctcpqueue, server->ctcpqueue);
    return 1;
}

/* Send CTCP reply with flood protection */
void ctcp_send_reply(SERVER_REC *server, char *data)
{
    g_return_if_fail(server != NULL);
    g_return_if_fail(data != NULL);

    if (g_list_length(server->ctcpqueue) < MAX_CTCP_QUEUE)
        server->ctcpqueue = g_list_append(server->ctcpqueue, g_strdup(data));
}

/* CTCP sound */
int ctcp_sound(char *sender, char *target, char *data)
{
    return 1;
}

/* CTCP version */
int ctcp_version(char *sender, char *target, char *data)
{
    char tmp[100];

    g_return_val_if_fail(sender != NULL, 1);

    sprintf(tmp, "NOTICE %s :\001VERSION %s\001", sender, IRC_VERSION);
    ctcp_send_reply(eserver, tmp);
    return 1;
}

/* CTCP ping */
int ctcp_ping(char *sender, char *target, char *data)
{
    char tmp[512];

    g_return_val_if_fail(sender != NULL, 1);
    g_return_val_if_fail(data != NULL, 1);

    sprintf(tmp, "NOTICE %s :\001PING %s\001", sender, data);
    ctcp_send_reply(eserver, tmp);
    return 1;
}

/* CTCP action = /ME command */
int ctcp_action(char *sender, char *target, char *data)
{
    CHAN_REC *chan;

    g_return_val_if_fail(sender != NULL, 1);
    g_return_val_if_fail(target != NULL, 1);
    g_return_val_if_fail(data != NULL, 1);

    if (*target != '#' && *target != '&')
    {
        /* private or dcc action */
        if (*target == '=')
            drawtext(eserver, target, LEVEL_DCC, "%7 (*dcc*) %s %8%s\n", sender, data); /* private */
        else
            drawtext(eserver, target, LEVEL_MSGS, "%7 (*) %s %8%s\n", sender, data); /* private */
    }
    else
    {
        /* channel action */
        chan = channel_joined(eserver, target);

        if (chan != NULL && chan->window->curchan != NULL &&
            strcasecmp(target, chan->window->curchan->name) == 0)
            drawtext(eserver, target, LEVEL_PUBLIC, "%7 * %s %8%s\n", sender, data); /* current channel */
        else
            drawtext(eserver, target, LEVEL_PUBLIC, "%7 * %s%n:%2%s %8%s\n", sender, target, data); /* some channel */

        if (chan != NULL && !chan->new_data && chan->window != curwin)
        {
            chan->new_data = 1;
            gui_channel_hilight(chan);
        }
    }
    return 0;
}

/* CTCP DCC */
int ctcp_dcc(char *sender, char *target, char *data)
{
    g_return_val_if_fail(sender != NULL, 1);
    g_return_val_if_fail(data != NULL, 1);

    return dcc_handle_ctcp(sender, data);
}

/* CTCP reply */
int ctcp_reply(char *sender, char *data)
{
    char *ptr;

    g_return_val_if_fail(sender != NULL, 0);
    g_return_val_if_fail(data != NULL, 0);

    ptr = strchr(data, ' ');
    if (ptr != NULL) *ptr++ = '\0'; else ptr = "";

    if (strcmp(data, "DCC") == 0)
        return dcc_reply(sender, ptr);

    if (!strcmp(data, "PING") && atoi(ptr) > 0 && atoi(ptr) < (int)time(NULL)) drawtext(eserver, NULL, LEVEL_CTCP, IRCTXT_CTCP_PING_REPLY, sender, ((int)time(NULL))-atoi(ptr));
    else drawtext(eserver, NULL, LEVEL_CTCP, IRCTXT_CTCP_REPLY, data, sender, ptr);

    return 1;
}

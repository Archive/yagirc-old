#include "config.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <glib.h>
#include <gtk/gtk.h>

#include "data.h"
#include "irc.h"
#include "gui_urllist.h"
#include "misc.h"

static int doubleclick;
static int select_row;

char *formaturl(URL_REC *url)
{
  char *temp = g_malloc(256);
  snprintf(temp, 255, "%s [%s@%s]\n", url->url, url->nick, url->channel);
  return temp;
}

void graburls(char *data, char *nick, char *channel)
{
  char *http, *ftp, *email, *temp;

  if (data == NULL) return;

  temp = strstr(data, "http://");
  if (temp != NULL) http = strdup(temp);
  else 
    {
      if (!strncmp(data, "http://", 7)) http = strdup(data);
      else http = NULL;
    }
  if (http != NULL)
    {
       temp = strchr(http, ' ');
       if (temp != NULL) *temp = '\0';
       catalogurl(http, nick, channel);
   }

  temp = strstr(data, "ftp://");
  if (temp != NULL) ftp = strdup(temp);
  else 
    {
      if (!strncmp(data, "ftp://", 6)) ftp = strdup(data);
      else ftp = NULL;
    }
  if (ftp != NULL)
    {
      temp = strchr(ftp, ' ');
      if (temp != NULL) *temp = '\0';
      catalogurl(ftp, nick, channel);
    }

/*
  email = strstr(data, "@");
  if (email != NULL)
    {
    }
*/
}

void catalogurl(char *data, char *nick, char *channel)
{
  URL_REC *temp;

  /* dont catalog if url catcher is turned off */
  if (!global_settings->urlcatch) return;

  temp = g_malloc(sizeof(URL_REC));

  if (channel == NULL) channel = strdup(nick);

  temp->url = strdup(data);
  temp->nick = strdup(nick);
  temp->channel = strdup(channel);

  urllist = g_list_append(urllist, temp);

  drawtext(NULL, NULL, LEVEL_YAGNOTICE, formaturl(temp));
}

/* browse to url */
int url_browse(char *url)
{
  int ret;
  char *tempexe;
  if (global_settings->browserpath == NULL || url == NULL) return(0);
  tempexe = g_new0(char, strlen(global_settings->browserpath)+strlen(url)+5);
  sprintf(tempexe, "%s %s &", global_settings->browserpath, url);
  ret = system(tempexe);
  g_free(tempexe);
  return(ret);
}

/* add a url to the url catcher window */
void gui_urllist_add(URLWIN_REC *urlwin, URL_REC *url)
{
  char *titles[] = { url->url, url->nick, url->channel };
  g_return_if_fail(urlwin != NULL && url != NULL);
  gtk_clist_append(GTK_CLIST(urlwin->list), titles);
}

void gui_urllist_select(GtkWidget *clist, gint row, gint column, GdkEventButton *event, gpointer data)
{
  select_row = row;
}

void gui_urllist_press(GtkCList *list, GdkEventButton *event)
{
  int ass;
  gchar *url;
  if (event->button == 1) {
    if (!doubleclick) doubleclick = 1;
    else {
      doubleclick = 0;
      gtk_clist_get_text(list, select_row, 0, &url);
      ass = url_browse(url);
      if (ass < 0 || ass == 127) drawtext(NULL, NULL, LEVEL_YAGNOTICE, "Error browsing URL: %s\n", strerror(errno));
    }
  }
}

void gui_urllist_release(GtkCList *list, GdkEventButton *event)
{
  if (event->button == 1) doubleclick = 0;
}

/* create window with caught urls in it */
void gui_urllist_show(GList *urls)
{
  GList *url;
  URLWIN_REC *urlwin;
  char *titles[] = { "URL", "Nick", "Channel" };

  if (urls == NULL)
  {
    drawtext(NULL, NULL, LEVEL_YAGNOTICE, "No URLs to list!@~\n");
    return;
  }

  /* setup the widgetz */
  urlwin = g_new0(URLWIN_REC, 1);
  urlwin->urls = urls;
  urlwin->win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  urlwin->list = gtk_clist_new_with_titles(3, titles);
  urlwin->mainbox = gtk_vbox_new(FALSE, 0);
  urlwin->buttonbox = gtk_hbox_new(TRUE, 5);
  urlwin->clearbutt = gtk_button_new_with_label("Clear");
  urlwin->closebutt = gtk_button_new_with_label("Close");

  /* configure the list box */
  gtk_clist_set_selection_mode(GTK_CLIST(urlwin->list), GTK_SELECTION_BROWSE);
  gtk_clist_set_policy(GTK_CLIST(urlwin->list), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_clist_column_titles_passive(GTK_CLIST(urlwin->list));
  gtk_clist_set_column_width(GTK_CLIST(urlwin->list), 0, 140);
  gtk_clist_set_column_width(GTK_CLIST(urlwin->list), 1, 60);

  /* configure the other shit */
  gtk_object_set_data(GTK_OBJECT(urlwin->win), "rec", urlwin);
  gtk_window_set_title(GTK_WINDOW(urlwin->win), "URL Listing");
  gtk_signal_connect(GTK_OBJECT(urlwin->win), "delete_event", GTK_SIGNAL_FUNC(gui_urllist_kill), NULL);
  gtk_signal_connect_object(GTK_OBJECT(urlwin->closebutt), "clicked", GTK_SIGNAL_FUNC(gui_urllist_kill), GTK_OBJECT(urlwin->win));
  gtk_signal_connect_object(GTK_OBJECT(urlwin->clearbutt), "clicked", GTK_SIGNAL_FUNC(gui_urllist_clear), GTK_OBJECT(urlwin->win));
  gtk_signal_connect(GTK_OBJECT(urlwin->list), "button_press_event", GTK_SIGNAL_FUNC(gui_urllist_press), NULL);
  gtk_signal_connect(GTK_OBJECT(urlwin->list), "button_release_event", GTK_SIGNAL_FUNC(gui_urllist_release), NULL);
  gtk_signal_connect(GTK_OBJECT(urlwin->list), "select_row", GTK_SIGNAL_FUNC(gui_urllist_select), NULL);

  /* lay out widgets */
  gtk_box_pack_start(GTK_BOX(urlwin->buttonbox), urlwin->clearbutt, TRUE, TRUE, 5);
  gtk_box_pack_start(GTK_BOX(urlwin->buttonbox), urlwin->closebutt, TRUE, TRUE, 5);
  gtk_box_pack_start(GTK_BOX(urlwin->mainbox), urlwin->list, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(urlwin->mainbox), urlwin->buttonbox, FALSE, TRUE, 0);
  gtk_container_add(GTK_CONTAINER(urlwin->win), urlwin->mainbox);

  /* add urls to the listbox */
  GLIST_FOREACH(url, urls)
  {
    gui_urllist_add(urlwin, url->data);
  }

  /* display the widgets */
  gtk_widget_show(urlwin->list);
  gtk_widget_show(urlwin->clearbutt);
  gtk_widget_show(urlwin->closebutt);
  gtk_widget_show(urlwin->buttonbox);
  gtk_widget_show(urlwin->mainbox);
  gtk_widget_set_usize(urlwin->win, 300, 200);
  gtk_widget_show(urlwin->win);
}

/* destroy url catcher window */
void gui_urllist_kill(GtkWidget *widget, void *dizata)
{
  URLWIN_REC *rec = gtk_object_get_data(GTK_OBJECT(widget), "rec");
  gtk_widget_destroy(rec->win);
  rec->win = NULL; rec->list = NULL; rec->closebutt = NULL; rec->clearbutt = NULL; rec->mainbox = NULL; rec->buttonbox = NULL; rec->urls = NULL;
  g_free(rec);
}

/* clear url list */
void gui_urllist_clear(GtkWidget *widget, gpointer data)
{
  URLWIN_REC *rec = gtk_object_get_data(GTK_OBJECT(widget), "rec");
  /* if (rec->urls != NULL) g_list_free(rec->urls); */
  rec->urls = NULL;
  gtk_clist_clear(GTK_CLIST(rec->list));
}

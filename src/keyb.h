#include "config.h"

#ifdef __EMX__
#  include <stdlib.h>
#  define key_hit() _read_kbd(0, 0, 0)
#  define key_get() _read_kbd(0, 1, 0)
#elif defined (UNIX)
int key_hit(void);
int key_get(void);
#else
#  include <conio.h>
#  define key_hit kbhit
#  define key_get getch
#endif

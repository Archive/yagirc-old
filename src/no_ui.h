#include "config.h"

#include "data.h"

#ifndef __NO_UI_H
#define __NO_UI_H

/* Input functions */
typedef void (*GUI_INPUT_FUNC)(void *);
#define GUI_INPUT_READ 1
#define GUI_INPUT_WRITE 2
int gui_input_add(int fh, int mode, GUI_INPUT_FUNC func, void *data);
void gui_input_remove(int tag);

/* Timeout functions */
typedef void (*GUI_TIMEOUT_FUNC)(void *);

int gui_timeout_new(int ms, GUI_TIMEOUT_FUNC func, void *data);
void gui_timeout_remove(int tag);

#ifdef USE_TEXTUI
#include "textui.h"
#else
typedef void GUI_WINDOW_REC;
typedef void GUI_CHAN_REC;
typedef void GUI_DCC_REC;
#endif

extern int default_color, default_bgcolor;
extern int yag_colors[16];

enum
{
    BLACK,
    BLUE,
    GREEN,
    CYAN,
    RED,
    MAGENTA,
    YELLOW,
    WHITE,
    BBLACK,
    BBLUE,
    BGREEN,
    BCYAN,
    BRED,
    BMAGENTA,
    BYELLOW,
    BWHITE,
};

#define DEFAULT_COLOR WHITE

/* Initialize IRC window */
void gui_window_init(WINDOW_REC *win, WINDOW_REC *parent);

/* Deinitialize IRC window */
void gui_window_deinit(WINDOW_REC *win);

/* Clear all text from window */
void gui_window_clear(WINDOW_REC *win);

/* IRC channel/window changed, update everything you can think of.. */
void gui_window_update(WINDOW_REC *win);

/* Change focus to specified window */
void gui_window_select(WINDOW_REC *win);

/* Change to any new window in same top window */
void gui_window_select_new(WINDOW_REC *win);

/* Quit program requested */
void gui_exit(void);

/* Inform GUI what connection state we are in - 0 = disconnected, 1 = socket
   connected, 2 = IRC server ready */
void gui_connected(SERVER_REC *serv, int state);

/* Someone in notify list joined IRC */
void gui_notify_join(char *nick);
/* Someone in notify list left IRC */
void gui_notify_part(char *nick);

/* Toggle window autoraising */
void gui_set_autoraise(WINDOW_REC *win, int on);

/* Update status bar */
void gui_update_statusbar(WINDOW_REC *win);

/* Set away state */
void gui_set_away(int away);

typedef void (*GUI_PASSWORD_FUNC) (char *, void *);

/* Create dialog to ask password */
void gui_ask_password(char *label, GUI_PASSWORD_FUNC func, void *data);

/* Change channel button label */
void gui_channel_change_name(CHAN_REC *chan, char *name);

#include "gui_dcc.h"
#include "gui_setup.h"
#include "gui_channels.h"
#include "gui_nicklist.h"

#endif

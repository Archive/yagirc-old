#ifndef __SCRIPT_TXT_H
#define __SCRIPT_TXT_H

#include "gui.h"
#include "intl.h"

static char *  IRCTXT_SCRIPT_NOT_FOUND=N_("Script file not found: %_%s\n");
static char *  IRCTXT_SCRIPT_LOAD_ERROR=N_("Error running script: %_%s\n");

#endif
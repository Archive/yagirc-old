#ifndef __EVENTS_NUMERIC_TXT_H
#define __EVENTS_NUMERIC_TXT_H

#include "gui.h"
#include "intl.h"

static char *  IRCTXT_AWAY=N_("You have been marked as being away\n");
static char *  IRCTXT_BANLIST=N_("%!%s%!: ban %_%s%_, set by %!%s%! %_%lu%_ secs ago\n");
static char *  IRCTXT_CANNOT_JOIN=N_("Cannot join to channel %_%s%_ (%!%s%!)\n");
static char *  IRCTXT_CHANNEL_CREATED=N_("Channel %_%s%_ created %s\n");
static char *  IRCTXT_CHANNEL_MODE=N_("mode/%_%s%_ [%!%s%!]\n");
static char *  IRCTXT_ENDOFNAMES=N_("%_%s%_: End of /NAMES list\n");
static char *  IRCTXT_END_OF_WHO=N_("End of /WHO list\n");
static char *  IRCTXT_END_OF_WHOIS=N_("End of /WHOIS\n");
static char *  IRCTXT_NAMES=N_("%_%s%_ names: %s\n");
static char *  IRCTXT_NICK_IN_USE=N_("Nick %_%s%_ is already in use\n");
static char *  IRCTXT_NICK_UNAVAILABLE=N_("Nick %_%s%_ is temporarily unavailable\n");
static char *  IRCTXT_ONLINE=N_("Users online: %_%s\n");
static char *  IRCTXT_TOPIC=N_("Topic for %_%s%_: %!%s\n");
static char *  IRCTXT_TOPIC_INFO=N_("Topic set by %_%s%_ [%!%s%!]\n");
static char *  IRCTXT_UNAWAY=N_("You are no longer marked as being away\n");
static char *  IRCTXT_WHOIS=N_("%s is %s@%s (%s)\n");
static char *  IRCTXT_WHOIS_IDLE=N_("%s has been idle %d hours %d mins %d secs");
static char *  IRCTXT_WHOIS_SIGNON=N_(" (signon: %s)");
static char *  IRCTXT_WHOIS_SERVER=N_("%s using %s (%s)\n");
static char *  IRCTXT_WHOIS_OPER=N_("%s is an IRC operator\n");
static char *  IRCTXT_WHOIS_CHANNELS=N_("%s on %s\n");

#endif
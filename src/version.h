#include "config.h"

#ifndef __VERSION_H
#define __VERSION_H

#define IRC_VERSION_NUMBER "v0.65.7"
#define IRC_VERSION "yagIRC " IRC_VERSION_NUMBER

#endif

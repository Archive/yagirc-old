/* SkyReader v2.00 - Copyright (c) 1997 Timo Sirainen */

/* con_lnx.cpp : Linux specific console management, ncurses v4.0 is used. */

#include "config.h"

#ifdef USE_TEXTUI
#include <stdio.h>
#include <string.h>

#include <ncurses.h>

#include "screen.h"

static int redraw_screen;
static int curxpos, curypos;

/******* Screen *******/

int console_init(void)
{
    char ansi_tab[8] = { 0, 4, 2, 6, 1, 5, 3, 7 };
    int num;

    if (!initscr()) return 0;

    raw(); halfdelay(1); noecho(); start_color();

    for (num = 1; num < COLOR_PAIRS; num++)
        init_pair(num, ansi_tab[num & 7], ansi_tab[num >> 3]);

    curxpos = scrwidth-1;
    curypos = scrlen-1;

    redraw_screen = 0;
    return 1;
}

void console_deinit(void)
{
    endwin();
}

inline void set_color(unsigned char col)
{
    register unsigned long bcol = 0;

    if ((col & 0x0f) == 8)
    {
        attroff(A_BOLD);
        bcol = A_DIM;
    }
    else
    {
        attroff(A_DIM);
        if (col & 8) bcol |= A_BOLD; else attroff(A_BOLD);
    }
    if ((col & 0x77) == 0) col |= 7;
    if (col & 128) bcol |= A_BLINK; else attroff(A_BLINK);
    attron(COLOR_PAIR((col&7) + (col&0x70)/2) | bcol);
}

void draw_screen(int x1, int y1, int x2, int y2)
{
    int x, xlen, oldcol, col;
    unsigned char str[2];
    SCR_CELL *cell;

    x1--; y1--; x2--; y2--;

    str[1] = '\0'; xlen = x2-x1+1; oldcol = -1;
    for (; y1 <= y2; y1++)
    {
        move(y1, x1);
        cell = screen + (x1+y1*scrwidth);
        for (x = xlen; x > 0; x--, cell++)
        {
            col = (unsigned char) (*cell >> CHAR_BITS);
            if (oldcol != col) { oldcol = col; set_color(col); }
            if ((*cell & 0xffffff) == 0) *cell |= ' ';
            /*if (((unsigned long) *cell & 0x80000080) != 0)
            {
                str[0] = (unsigned char) *cell;
                addstr((char *) str);
            }
            else*/
                addch(*cell & 0xffffff);
        }
    }

    move(curypos, curxpos);
    refresh();
    redraw_screen = 1;
}

void setcursor(int x, int y)
{
    curxpos = x-1; curypos = y-1;
    move(curypos, curxpos);
    refresh();
}

void detect_screen_size(int *scrwidth, int *scrsize)
{
    *scrsize = LINES;
    *scrwidth = COLS;
}


/******* Keyboard *******/

static char waiting = 0;
static int _meta = 0;

int key_hit(void)
{
    int hit;

    hit = wgetch(stdscr);
    if (hit != ERR)
        ungetch(hit);
    else
    {
        if (redraw_screen)
        {
            refresh();
            redraw_screen = 0;
        }
    }

    return hit != ERR;
}

inline int chr_in(void)
{
    while (!key_hit()) ;

    return wgetch(stdscr);
}

int key_get(void)
{
    char alt_table[] = "qwertyuiopasdfghjklzxcvbnm";

    int ch, num;

    if (waiting != 0)
    {
        ch = waiting;
        waiting = 0;
        if (ch == 27) goto __esc;
        return ch;
    }

    ch = chr_in();
    if (ch == 24) { _meta = 1; return 24; } /* Ctrl-X */
    if (ch == 10) ch = 13;
    if (ch == 127) return 8; /* Backspace */

    if (_meta) goto __alt;

    if (ch == 27)
    {
    __esc:
        /* Scan code? */
        halfdelay(0);
        num = wgetch(stdscr);
        if (num == ERR)
        {
            halfdelay(1);
            return ch;
        }

        /* Yep.. */
        ch = (char) num;

        if (ch == '[')
        {
            if (!key_hit()) return 27;
            ch = key_get();

            if (ch >= '0' && ch <= '9')
            {
                num = 0;
                while (ch >= '0' && ch <= '9')
                {
                    num = num*10+ch-'0';
                    if (!key_hit()) return 27;
                    ch = key_get();
                }

                switch (num)
                {
                    case 1:
                        /* Home */
                        waiting = 'G';
                        return 0;
                    case 2:
                        /* Insert */
                        waiting = 'R';
                        return 0;
                    case 3:
                        /* Delete */
                        waiting = 'S';
                        return 0;
                    case 4:
                        /* End */
                        waiting = 'O';
                        return 0;
                    case 5:
                        /* PgUp */
                        waiting = 'I';
                        return 0;
                    case 6:
                        /* PgDn */
                        waiting = 'Q';
                        return 0;
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                        /* F6..F10 */
                        waiting = 64+num-17;
                        return 0;
                    case 23:
                        /* F11 */
                        waiting = 133;
                        return 0;
                    case 24:
                        /* F12 */
                        waiting = 134;
                        return 0;
                    case 25:
                        /* Shift-F3 */
                        waiting = 86;
                        return 0;
                    case 26:
                        /* Shift-F4 */
                        waiting = 87;
                        return 0;
                    case 28:
                        /* Shift-F5 */
                        waiting = 88;
                        return 0;
                    case 29:
                        /* Shift-F6 */
                        waiting = 89;
                        return 0;
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                        /* Shift-F7..Shift-F10 */
                        waiting = 90+num-31;
                        return 0;
                    default:
                        waiting = 200+num;
                        return 0;
                }
            }

            switch (ch)
            {
                case 'A':
                    /* Cursor up */
                    waiting = 'H';
                    return 0;
                case 'B':
                    /* Down */
                    waiting = 'P';
                    return 0;
                case 'C':
                    /* Right */
                    waiting = 'M';
                    return 0;
                case 'D':
                    /* Left */
                    waiting = 'K';
                    return 0;
                case '[':
                    if (!key_hit()) return '[';
                    ch = key_get();
                    if (ch >= 'A' && ch <= 'E')
                    {
                        waiting = 59+ch-'A';
                        return 0;
                    }
                    waiting = ch;
                    return 0;
                default:
                    waiting = ch;
                    return 0;
            }
        }
        else
        {
        __alt:
            _meta = 0;
            if (ch >= 'a' && ch <= 'z')
            {
                /* Alt-key */
                num = (int) (strchr(alt_table,ch)-alt_table);
                if (num < 10) num += 16;
                else if (num < 19) num += 30-10;
                else num += 44-19;
                waiting = num;
                return 0;
            }
            if (ch >= '1' && ch <= '9')
            {
                waiting = ch-'1'+120;
                return 0;
            }
            switch (ch)
            {
                case 10:
                    waiting = 28;
                    return 0;
                case 8:
                    waiting = 14;
                    return 0;
                case 9:
                    waiting = 165;
                    return 0;
                case 27:
                    waiting = 27;
                    return 27;
                case '0':
                    waiting = 129;
                    return 0;
                default:
                    waiting = ch;
                    return 0;
            }
        }
    }

    return ch;
}
#endif

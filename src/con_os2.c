/* SkyReader v2.00 - Copyright (c) 1997 Timo Sirainen */

/* con_os2.cpp : OS/2 specific console management */

#include "config.h"
#define INCL_NOPMAPI
#define INCL_MOU
#define INCL_VIO
#include <os2.h>
#include <string.h>

#include "screen.h"
#include "mouse.h"


/******* Screen *******/

static VIOCURSORINFO old_cursor;

int console_init(void)
{
    init_mouse(); show_mouse();
    VioGetCurType(&old_cursor, 0);
    return 1;
}

void console_deinit(void)
{
    deinit_mouse();
}

void draw_screen(int x1, int y1, int x2, int y2)
{
    x1--; y1--; y2--;

    hide_mouse(x1, y1, x2-1, y2); /* Hide mouse when drawing to screen. */

    x2 = (x2-x1)*2;

#ifdef __EMX__
    for (; y1 <= y2; y1++)
        VioWrtCellStr((unsigned char *) (screen+x1+y1*scrwidth), (unsigned short) x2, (unsigned short) y1, (unsigned short) x1, 0);
#else
    for (; y1 <= y2; y1++)
        VioWrtCellStr((char *) (screen+x1+y1*scrwidth), (unsigned short) x2, (unsigned short) y1, (unsigned short) x1, 0);
#endif

    show_mouse();
}

void setcursor(int x, int y)
{
    VioSetCurPos((unsigned short) (x-1), (unsigned short) (y-1), 0);
}

void hide_cursor(void)
{
    VIOCURSORINFO vio_curs;

    vio_curs.yStart = 0;
    vio_curs.cEnd = 0;
    vio_curs.cx = 0;
    vio_curs.attr = 0xffff;
    VioSetCurType(&vio_curs, 0);
}

void show_cursor(void)
{
    VioSetCurType(&old_cursor, 0);
}

void set_cursorsize(int cstart,int cend)
{
    VIOCURSORINFO vio_curs;

    vio_curs.yStart = (unsigned short) cstart;
    vio_curs.cEnd = (unsigned short) cend;
    vio_curs.cx = 0;
    vio_curs.attr = 0;
    VioSetCurType(&vio_curs, 0);
}

void get_cursorsize(int *cstart, int *cend)
{
    VIOCURSORINFO vio_curs;

    VioGetCurType(&vio_curs, 0);
    *cstart = vio_curs.yStart;
    *cend = vio_curs.cEnd;
}

void detect_screen_size(int *scrwidth, int *scrsize)
{
    VIOMODEINFO OldMode;

    OldMode.cb = sizeof(VIOMODEINFO);
    VioGetMode(&OldMode,0);
    *scrsize = OldMode.row;
    *scrwidth = OldMode.col;
}


/******* Mouse *******/


static HMOU hmouse;

int init_mouse(void)
{
    if (MouOpen(NULL, &hmouse) != 0)
    {
        hmouse = 0xffff;
        return 0;
    }

    return 1;
}

void deinit_mouse(void)
{
    if (hmouse != 0xffff)
    {
        MouClose(hmouse);
        hmouse = 0xffff;
    }
}

void show_mouse(void)
{
    MouDrawPtr(hmouse);
}

void hide_mouse(int x1, int y1, int x2, int y2)
{
    NOPTRRECT area;

    area.row = (USHORT) y1;
    area.col = (USHORT) x1;
    area.cRow = (USHORT) y2;
    area.cCol = (USHORT) x2;
    MouRemovePtr(&area, hmouse);
}

void mouse_stat(int *x, int *y, int *but)
{
    USHORT wait = 0;
    MOUEVENTINFO evt;

    MouReadEventQue(&evt, &wait, hmouse);
    *x = evt.col; *y = evt.row; *but = 0;

    if (evt.fs & 0x0004) *but |= MOUSE_BUT1_DOWN;
    if (evt.fs & 0x0010) *but |= MOUSE_BUT2_DOWN;
    if (evt.fs & 0x0040) *but |= MOUSE_BUT3_DOWN;
}


#include "config.h"

#ifdef USE_TEXTUI
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <pwd.h>
#include <unistd.h>

#include <glib.h>

#include "os.h"
#include "irc.h"
#include "commands.h"
#include "script.h"

#include "keyb.h"
#include "screen.h"
#include "intl.h"

#define MIN_TIMEOUT 100

typedef struct
{
    int handle;
    int mode;
    int tag;
    GUI_INPUT_FUNC func;
    void *data;
}
INPUT_REC;

typedef struct
{
    int ms;
    int gone;
    int tag;
    GUI_TIMEOUT_FUNC func;
    void *data;
}
TIMEOUT_REC;

static GSList *inputlist, *timelist;
static int maxhandle, mintimeout, incount, timecount;

static int quit_program;

char *default_server;
int default_server_port;
char *default_nick;
char *real_name;
char *user_name;

int default_fgcolor, default_bgcolor;
int yag_colors[16];

/* Create DCC transfer window */
void gui_dcc_init(DCC_REC *dcc) {}
/* Clear all text from window */
void gui_window_clear(WINDOW_REC *win) {}
/* Update DCC transfer window */
void gui_dcc_update(DCC_REC *dcc) {}
/* Close DCC transfer window */
void gui_dcc_close(DCC_REC *dcc) { g_free(dcc); }
/* Destroy DCC transfer window */
void gui_dcc_force_close(DCC_REC *dcc) {}

/* DCC chat connection established - do something.. */
void gui_dcc_chat_init(DCC_REC *dcc) {}
/* Text received from DCC chat - write to screen */
void gui_dcc_chat_write(DCC_REC *dcc, char *str) {}
/* Hilight channel button */
void gui_channel_hilight(CHAN_REC *chan) {}
/* Dehilight channel button */
void gui_channel_dehilight(CHAN_REC *chan) {}
/* Select channel in specific window, if channel is in some other window,
   move it from there */
void gui_select_channel(WINDOW_REC *win, CHAN_REC *chan) {}
/* Redraw nick list */
void gui_nicklist_redraw(WINDOW_REC *window) {}

/* Change nick name */
void gui_nick_change(SERVER_REC *server, char *from, char *to) {}
/* Change nick's mode (@, +) */
void gui_change_nick_mode(CHAN_REC *chan, char *nick, char type) {}
/* Someone joined some channel.. If nick==NULL, you joined */
void gui_channel_join(CHAN_REC *chan, char *nick) {}
/* Someone left some channel.. If nick==NULL, you left, if chan==NULL someone
   quit from IRC */
void gui_channel_part(CHAN_REC *chan, char *nick) {}

/* Toggle window autoraising */
void gui_set_autoraise(WINDOW_REC *win, int on) {}

/* Change topic of channel */
void gui_change_topic(CHAN_REC *chan) {}

/* Display channel modes dialog */
void gui_channel_modes(CHAN_REC *chan) {}

/* Create dialog to ask password */
void gui_ask_password(char *label, GUI_PASSWORD_FUNC func, void *data) {}

/* Change channel button label */
void gui_channel_change_name(CHAN_REC *chan, char *name) {}


static char ansitab[8] = { 0, 4, 2, 6, 1, 5, 3, 7 };

#define FLAG_BOLD 1
#define FLAG_REVERSE 2
#define FLAG_UNDERLINE 4

/* parse ANSI color string */
static char *convert_ansi(char *str, int *fgcolor, int *bgcolor, int *flags)
{
    char *start;
    int fg, bg, fl, num;

    if (*str != '[') return str;

    start = str;
    fg = *fgcolor-1; bg = *bgcolor; fl = *flags;
    if (bg != -1) bg--;

    str++; num = 0;
    for (;; str++)
    {
        if (*str == '\0') return start;

        if (isdigit(*str))
        {
            num = num*10 + (*str-'0');
            continue;
        }

        if (*str != ';' && *str != 'm') return start;

        switch (num)
        {
            case 0:
                /* reset colors back to default */
                fg = 7;
                bg = -1;
                break;
            case 1:
                /* hilight */
                fg |= 8;
                break;
            case 5:
                /* blink */
                bg = bg == -1 ? 8 : bg | 8;
                break;
            case 7:
                /* reverse */
                fl |= FLAG_REVERSE;
                break;
            default:
                if (num >= 30 && num <= 37)
                    fg = (fg & 0xf8) + ansitab[num-30];
                if (num >= 40 && num <= 47)
                {
                    if (bg == -1) bg = 0;
                    bg = (bg & 0xf8) + ansitab[num-40];
                }
                break;
        }
        num = 0;

        if (*str == 'm')
        {
            *fgcolor = fg+1;
            *bgcolor = bg == -1 ? -1 : bg+1;
            *flags = fl;
            str++;
            break;
        }
    }

    return str;
}

void new_line(WINDOW_REC *win)
{
    int visible;

    visible = win == curwin && curwin->gui->scrollpos+scrlen-3 >= curwin->gui->lines;

    win->gui->linepos = win->gui->curline;

    if (win->gui->printy < scrlen-3)
        win->gui->printy++;
    else
    {
        win->gui->scrollpos++;
        if (visible)
            scroll_up(1, 1, scrwidth, scrlen-3);
    }

    if (win->gui->lines < MAX_TEXTLINES_HISTORY)
    {
        win->gui->lines++;
    }
    else
    {
        win->gui->linelist = g_list_first(win->gui->linelist);
        g_free(win->gui->linelist->data);
        win->gui->linelist = g_list_remove_link(win->gui->linelist, win->gui->linelist);
    }
    win->gui->linelist = g_list_append(win->gui->linelist, g_strdup(win->gui->curline));

    if (visible)
        tbar(1, win->gui->printy, scrwidth, win->gui->printy, default_fgcolor+(default_bgcolor<<4));
    win->gui->printx = 1;
}

static int nonew = 0;

/* Write text to window */
static void printtext(char *str, WINDOW_REC *win)
{
    char *ptr, type, *tmp;
    int fg, bg;
    int fgcolor, bgcolor;
    int flags, visible;

    g_return_if_fail(str != NULL);
    g_return_if_fail(win != NULL);
    g_return_if_fail(win->gui != NULL);

    if (!nonew) new_line(win);

    visible = win == curwin && (nonew || curwin->gui->scrollpos+scrlen-3 >= curwin->gui->lines);

    flags = 0;
    fg = default_fgcolor; fgcolor = -2; bgcolor = -1; type = '\0';
    while (*str != '\0')
    {
        for (ptr = str; *ptr != '\0'; ptr++)
            if ((*ptr >= 2 && *ptr <= 5) || *ptr == 22 || *ptr == 27 || *ptr == 31)
            {
                type = *ptr;
                *ptr++ = '\0';
                break;
            }

        if (*str != '\0')
        {
            /*GdkFont *font;

            if (flags & FLAG_BOLD)
                font = flags & FLAG_UNDERLINE ? font_bolditalic : font_bold;
            else if (flags & FLAG_UNDERLINE)
                font = font_italic;
            else
                font = font_normal;*/

            if (flags & FLAG_REVERSE)
            {
                if (fgcolor == -2) bg = fg; else bg = fgcolor;
                fg = bgcolor == -1 ? default_bgcolor : bgcolor;
            }
            else
            {
                if (fgcolor != -2) fg = fgcolor;
                bg = bgcolor == -1 ? default_bgcolor : bgcolor;
            }

            tmp = str;
            while (tmp != NULL)
            {
                int len;

                len = strlen(tmp);
                if (win->gui->printx+len <= scrwidth)
                {
                    if (!nonew) win->gui->linepos += sprintf(win->gui->linepos, "\x05%c%s", fg+(bg<<4), tmp);
                    if (visible) writestr(win->gui->printx, win->gui->printy, tmp, fg+(bg<<4));
                    win->gui->printx += len;
                    tmp = NULL;
                }
                else
                {
                    /* we have to split line... */
                    char *pos;

                    for (pos = &tmp[scrwidth-win->gui->printx]; pos > tmp; pos--)
                        if (*pos == ' ') break;
                    if (pos == tmp)
                    {
                        /* raw split */
                        char ch;

                        ch = *pos;
                        *pos = '\0';
                        if (!nonew) win->gui->linepos += sprintf(win->gui->linepos, "\x05%c%s", fg+(bg<<4), tmp);
                        if (visible) writestr(win->gui->printx, win->gui->printy, tmp, fg+(bg<<4));
                        if (!nonew) new_line(win);
                        *pos = ch;
                    }
                    else
                    {
                        *pos++ = '\0';
                        if (!nonew) win->gui->linepos += sprintf(win->gui->linepos, "\x05%c%s", fg+(bg<<4), tmp);
                        if (visible) writestr(win->gui->printx, win->gui->printy, tmp, fg+(bg<<4));
                        if (!nonew) new_line(win);
                    }
                    tmp = pos;
                }
            }
        }
        if (*ptr == '\0') break;

        if (type == '\0')
        {
            /* ... */
        }
        else if (type == 2)
        {
            /* bold */
            flags ^= FLAG_BOLD;
            if (flags & FLAG_BOLD)
                fg |= 8;
            else
                fg &= ~8;
        }
        else if (type == 22)
        {
            /* reverse */
            flags ^= FLAG_REVERSE;
        }
        else if (type == 31)
        {
            /* underline, display as italic.. */
            flags ^= FLAG_UNDERLINE;
        }
        else if (type == 27)
        {
            /* ansi color code */
            ptr = convert_ansi(ptr, &fgcolor, &bgcolor, &flags);
        }
        else if (type == 4)
        {
            /* user specific colors */
            fg = yag_colors[((int) *ptr++)-1];
            fgcolor = -2;
            bgcolor = -1;
        }
        else if (type == 5)
        {
            /* user specific colors */
            fg = *ptr;
            bg = *ptr >> 4;
            ptr++;
            fgcolor = -2;
            bgcolor = -1;
        }
        else if (type == 3)
        {
            /* color! */
            if (*ptr < 16)
            {
                fgcolor = *ptr++;
                bgcolor = -1;
            }
            else
            {
                fgcolor = 0; bgcolor = -1;
                if (!isdigit(*ptr))
                    fgcolor = -2;
                else
                {
                    /* foreground color */
                    fgcolor = *ptr++-'0';
                    if (isdigit(*ptr))
                        fgcolor = fgcolor*10 + (*ptr++-'0');
                    fgcolor++;
                    if (*ptr == ',')
                    {
                        /* back color */
                        bgcolor = 0;
                        if (!isdigit(*++ptr))
                            bgcolor = -1;
                        else
                        {
                            bgcolor = *ptr++-'0';
                            if (isdigit(*ptr))
                                bgcolor = bgcolor*10 + (*ptr++-'0');
                            bgcolor++;
                        }
                    }
                }
            }
        }

        str = ptr;
    }
    draw_screen(1, 1, scrwidth, scrlen-3);
}

/* Initialize IRC window */
void gui_window_init(WINDOW_REC *win, WINDOW_REC *parent)
{
    win->drawfunc = (DRAW_FUNC) printtext;
    win->drawfuncdata = win;

    win->gui = g_new0(GUI_WINDOW_REC, 1);

    win->gui->curline = (char *) malloc(scrwidth*3);

    win->gui->printx = 1;
    win->gui->printy = 1;

    gui_window_select(win);
}

/* Deinitialize IRC window */
void gui_window_deinit(WINDOW_REC *win)
{
    g_free(win->gui->curline);
    g_free(win->gui);
}

/* IRC channel/window changed, update everything you can think of.. */
void gui_window_update(WINDOW_REC *win) {}

/* Change focus to specified window */
void gui_window_select(WINDOW_REC *win)
{
    GList *pos;
    char *tmp;
    int n, oldy;

    irc_window_focus(win);

    tbar(1, 1, scrwidth, scrlen-3, default_fgcolor+(default_bgcolor<<4));
    nonew = 1; oldy = win->gui->printy;
    pos = g_list_nth(win->gui->linelist, win->gui->scrollpos);
    for (n = 1; n <= scrlen-3 && pos != NULL; n++, pos = pos->next)
    {
        tmp = g_strdup(pos->data);
        win->gui->printx = 1; win->gui->printy = n;
        printtext(tmp, win);
        g_free(tmp);
    }
    if (n <= scrlen-3)
    {
        tmp = g_strdup(win->gui->curline);
        win->gui->printx = 1; win->gui->printy = n;
        printtext(tmp, win);
        g_free(tmp);
    }
    win->gui->printy = oldy;
    nonew = 0;
    draw_screen(1, 1, scrwidth, scrlen-3);

    gui_update_statusbar(curwin);
}

/* Change to any new window in same top window */
void gui_window_select_new(WINDOW_REC *win) {}

/* Quit program requested */
void gui_exit(void)
{
    quit_program = 1;
}

/* Inform GUI what connection state we are in - 0 = disconnected, 1 = socket
   connected, 2 = IRC server ready */
void gui_connected(SERVER_REC *serv, int state)
{
    gui_update_statusbar(NULL);
}

/* Someone in notify list joined IRC */
void gui_notify_join(char *nick) {}
/* Someone in notify list left IRC */
void gui_notify_part(char *nick) {}

/* Update status bar */
void gui_update_statusbar(WINDOW_REC *win)
{
    SERVER_REC *server;
    char str[200];
    int connected;
    GList *tmp;
    int windows;
    char *act, *ptr;

    if (win != NULL && win != curwin)
    {
        /* window is hidden - status doesn't need to be displayed */
        return;
    }

    /* find which windows have new text */
    windows = g_list_length(winlist);

    if (windows == 0)
        act = NULL;
    else
    {
        act = g_new0(char, windows*(sprintf(str, "%d", windows)+1)+10);

        ptr = act;
        for (tmp = g_list_first(winlist); tmp != NULL; tmp = tmp->next)
        {
            WINDOW_REC *w;

            w = (WINDOW_REC *) tmp->data;
            if (w->new_data)
            {
                if (act == ptr) ptr += sprintf(ptr, "[ act: ");
                ptr += sprintf(ptr, "%d ", w->num);
            }
        }
        if (ptr != act) strcpy(ptr, "]");
    }

    server = curwin->curchan == NULL ? curwin->defserv : curwin->curchan->server;
    connected = server != NULL && server->connected;
    g_snprintf(str, sizeof(str), "[ window %d ] [ %s ] [ %s%s ] %s",
               curwin->num,
               connected ? server->nick : default_nick,
               connected ? _("Connected to ") : _("Not connected"),
               connected ? server->name : "",
               act == NULL ? "" : act);

    str[scrwidth] = '\0';
    tbar(1, scrlen-1, scrwidth, scrlen-1, 7+(1<<4));
    writestr(1, scrlen-1, str, 7+(1<<4));
    draw_screen(1, scrlen-1, scrwidth, scrlen-1);

    if (act != NULL) g_free(act);
}

/* Set away state */
void gui_set_away(int away) {}

/* key pressed */
void key_click(void)
{
    static char buf[512];
    static int bufpos = 0;
    int key;

    key = key_get();
    switch (key)
    {
        case 0:
            key = key_get();
            switch (key)
            {
                case 'H':
                    {
                        GList *pos;

                        if (curwin->gui->scrollpos == 0) break;
                        curwin->gui->scrollpos--;
                        scroll_down(1, 1, scrwidth, scrlen-3);
                        curwin->gui->printy = 1; curwin->gui->printx = 1;
                        tbar(1, 1, scrwidth, 1, default_fgcolor+(default_bgcolor<<4));
                        pos = g_list_nth(curwin->gui->linelist, curwin->gui->scrollpos);
                        if (pos != NULL)
                        {
                            char *tmp;

                            tmp = g_strdup(pos->data);
                            nonew = 1;
                            printtext(tmp, curwin);
                            draw_screen(1, 1, scrwidth, scrlen);
                            nonew = 0;
                            g_free(tmp);
                        }
                        curwin->gui->printy = scrlen-3;
                        break;
                    }
                case 'P':
                    {
                        GList *pos;
                        char *tmp;

                        if (curwin->gui->scrollpos+scrlen-3 >= curwin->gui->lines) break;
                        curwin->gui->scrollpos++;
                        scroll_up(1, 1, scrwidth, scrlen-3);
                        curwin->gui->printy = scrlen-3; curwin->gui->printx = 1;
                        tbar(1, scrlen-3, scrwidth, scrlen-3, default_fgcolor+(default_bgcolor<<4));
                        pos = g_list_nth(curwin->gui->linelist, curwin->gui->scrollpos+scrlen-3);
                        nonew = 1;
                        if (pos != NULL)
                            tmp = g_strdup(pos->data);
                        else
                            tmp = g_strdup(curwin->gui->curline);
                        printtext(tmp, curwin);
                        g_free(tmp);
                        nonew = 0;
                        break;
                    }
                default:
                    if (key >= 120 && key <= 129)
                    {
                        GList *tmp;

                        tmp = g_list_nth(winlist, key-120);
                        if (tmp != NULL) gui_window_select((WINDOW_REC *) tmp->data);
                    }
                    break;
            }
            break;
        case 8:
            if (bufpos == 0) break;
            writechar(bufpos, scrlen, ' ', 7);
            draw_screen(bufpos, scrlen, bufpos, scrlen);
            setcursor(bufpos, scrlen);
            bufpos--;
            break;
        case 13:
            if (bufpos == 0) break;
            buf[bufpos] = '\0';
            irc_parse_outgoing(curwin->curchan, buf);
            bufpos = 0;
            tbar(1, scrlen, scrwidth, scrlen, 7);
            draw_screen(1, scrlen, scrwidth, scrlen);
            setcursor(1, scrlen);
            break;
        default:
            if (key >= 32)
            {
                buf[bufpos++] = key;
                writechar(bufpos, scrlen, key, 7);
                draw_screen(bufpos, scrlen, bufpos, scrlen);
                setcursor(bufpos+1, scrlen);
            }
            break;
    }
}

int gui_input_add(int fh, int mode, GUI_INPUT_FUNC func, void *data)
{
    INPUT_REC *inp;

    inp = g_new(INPUT_REC, 1);
    inp->handle = fh;
    inp->mode = mode;
    inp->func = func;
    inp->data = data;
    inp->tag = ++incount;
    if (fh > maxhandle) maxhandle = fh;

    inputlist = g_slist_append(inputlist, inp);
    return incount;
}

void gui_input_remove(int tag)
{
    GSList *tmp, *rec;
    int maxhandle;

    rec = NULL; maxhandle = -1;
    for (tmp = inputlist; tmp != NULL; tmp = tmp->next)
    {
        INPUT_REC *inp;

        inp = (INPUT_REC *) tmp->data;
        if (inp->tag == tag)
        {
            g_free(tmp->data);
            rec = tmp;
            break;
        }
        else
        {
            if (inp->handle > maxhandle) maxhandle = inp->handle;
        }
    }

    if (rec != NULL)
    {
        incount--;
        inputlist = g_slist_remove_link(inputlist, rec);
    }
}

int gui_timeout_new(int ms, GUI_TIMEOUT_FUNC func, void *data)
{
    TIMEOUT_REC *t;

    t = g_new(TIMEOUT_REC, 1);
    t->ms = ms;
    t->gone = 0;
    t->func = func;
    t->data = data;
    t->tag = ++timecount;
    if (ms < mintimeout) mintimeout = ms;
    timelist = g_slist_append(timelist, t);
    return timecount;
}

void gui_timeout_remove(int tag)
{
    GSList *tmp, *rec;

    rec = NULL; mintimeout = MIN_TIMEOUT;
    for (tmp = timelist; tmp != NULL; tmp = tmp->next)
    {
        TIMEOUT_REC *t;

        t = (TIMEOUT_REC *) tmp->data;
        if (t->tag == tag)
        {
            g_free(tmp->data);
            rec = tmp;
        }
        else
        {
            if (t->ms < mintimeout) mintimeout = t->ms;
        }
    }
    if (rec != NULL)
    {
        timecount--;
        timelist = g_slist_remove_link(timelist, rec);
    }
}

void gui_init(void)
{
    inputlist = timelist = NULL;
    maxhandle = -1; mintimeout = MIN_TIMEOUT; incount = 0; timecount = 0;
    quit_program = 0;

    init_screen();

    clrscr();
    tbar(1, scrlen-2, scrwidth, scrlen-1, (1<<4)+15);
    draw_screen(1, 1, scrwidth, scrlen);
    setcursor(1, scrlen);
}

void gui_deinit(void)
{
    g_slist_foreach(inputlist, (GFunc) g_free, NULL);
    g_slist_free(inputlist);

    g_slist_foreach(timelist, (GFunc) g_free, NULL);
    g_slist_free(timelist);

    g_free(user_name);
    g_free(real_name);
    g_free(default_nick);
    if (default_server != NULL) g_free(default_server);

    deinit_screen();
}

void main_loop(void)
{
    GSList *tmp;
    static fd_set infd, outfd;

    while (!quit_program)
    {
        struct timeval tv;

        while (key_hit())
        {
            key_click();
            if (quit_program) break;
        }

        FD_ZERO(&infd); FD_ZERO(&outfd);
        for (tmp = inputlist; tmp != NULL; tmp = tmp->next)
        {
            INPUT_REC *inp;

            inp = (INPUT_REC *) tmp->data;

            if (inp->mode == GUI_INPUT_READ)
                FD_SET(inp->handle, &infd);
            else
                FD_SET(inp->handle, &outfd);
        }

        tv.tv_sec = mintimeout/1000;
        tv.tv_usec = (mintimeout%1000)*1000;
        if (select(maxhandle+1, &infd, &outfd, NULL, &tv) > 0)
        {
            if (quit_program) break;
            for (tmp = inputlist; tmp != NULL; tmp = tmp->next)
            {
                INPUT_REC *inp;

                inp = (INPUT_REC *) tmp->data;
                if ((inp->mode == GUI_INPUT_READ && FD_ISSET(inp->handle, &infd)) ||
                    (inp->mode == GUI_INPUT_WRITE && FD_ISSET(inp->handle, &outfd)))
                {
                    inp->func(inp->data);
                    if (quit_program) break;
                }
            }
        }
        if (quit_program) break;

        for (tmp = timelist; tmp != NULL; tmp = tmp->next)
        {
            TIMEOUT_REC *t;

            t = (TIMEOUT_REC *) tmp->data;
            t->gone += mintimeout;
            if (t->ms >= t->gone)
            {
                t->gone = 0;
                t->func(t->data);
                if (quit_program) break;
            }
        }
    }
}

void gui_setup(void)
{
    struct passwd *entry;
    char *ptr;

    /* get user information */
    user_name = getenv("IRCUSER");
    real_name = getenv("IRCNAME");
    default_server = getenv("IRCSERVER");

    default_server_port = 6667;
    if (default_server != NULL)
    {
        /* get server port */
        ptr = strchr(default_server, ':');
        if (ptr != NULL)
        {
            *ptr = '\0';
            if (sscanf(ptr, "%d", &default_server_port) != 1)
                default_server_port = 6667;
        }
    }

    /* get passwd-entry */
    entry = getpwuid(getuid());
    if (entry != NULL)
    {
        user_name = entry->pw_name;
        if (real_name == NULL) real_name = entry->pw_gecos;
    }

    ptr = getenv("IRCNICK");
    if (ptr == NULL)
    {
        if (user_name != NULL)
            ptr = user_name;
        else
            ptr = "someone";
    }

    default_nick = ptr;
    if (user_name == NULL || *user_name == '\0') user_name = default_nick;
    if (real_name == NULL || *real_name == '\0') real_name = user_name;

    user_name = g_strdup(user_name);
    real_name = g_strdup(real_name);
    default_nick = g_strdup(default_nick);
    if (default_server != NULL) default_server = g_strdup(default_server);

    default_fgcolor = WHITE;
    default_bgcolor = BLACK;
    yag_colors[0] = CYAN;
    yag_colors[1] = BMAGENTA;
    yag_colors[2] = GREEN;
    yag_colors[3] = BMAGENTA;
    yag_colors[4] = MAGENTA;
    yag_colors[5] = RED;
    yag_colors[6] = BRED;
    yag_colors[7] = BMAGENTA;
    yag_colors[8] = MAGENTA;
    yag_colors[9] = GREEN;
    yag_colors[10] = BLACK;
    yag_colors[11] = BRED;
    yag_colors[12] = BBLUE;
    yag_colors[13] = GREEN;
    yag_colors[14] = CYAN;
    yag_colors[15] = default_fgcolor;
}

int main (int argc, char *argv[])
{
    WINDOW_REC *win;

    gui_setup();
    gui_init();
    irc_init();

    win = irc_window_new(NULL, NULL);

    main_loop();

    irc_deinit();
    gui_deinit();

    return 0;
}
#endif

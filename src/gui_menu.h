#include "config.h"

#ifndef __GUI_MENU_H
#define __GUI_MENU_H

#include "gui_menufactory.h"

/* add menuitem to menu */
void add_popup(GtkWidget *menu, char *label, void *func, void *data);
/* add submenu to menu */
void add_popup_sub(GtkWidget *menu, char *label, GtkWidget *submenu);

/* callback functions */
void menu_irc_connect(GtkWidget *widget, gpointer data);
void menu_irc_disconnect(GtkWidget *widget, gpointer data);
void menu_irc_setup(GtkWidget *widget, gpointer data);
void menu_irc_quit(GtkWidget *widget, gpointer data);
void menu_command_join(GtkWidget *widget, gpointer data);
void menu_command_part(GtkWidget *widget, gpointer data);
void menu_window_new(GtkWidget *widget, gpointer data);
void menu_window_new_hidden(GtkWidget *widget, gpointer data);
void menu_window_close(GtkWidget *widget, gpointer data);
void menu_window_select_server(GtkWidget *widget, gpointer data);
void menu_window_show_urllist(GtkWidget *widget, gpointer data);
void menu_help_about(GtkWidget *widget, gpointer data);

#endif

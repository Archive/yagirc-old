#include "config.h"

#ifndef __GUI_SETUP_LISTS_H
#define __GUI_SETUP_LISTS_H

void gui_setup_ignorelist(GtkWidget *vbox);
void gui_setup_notifylist(GtkWidget *vbox);

#endif

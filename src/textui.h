#include "config.h"

#ifndef __TEXTUI_H
#define __TEXTUI_H

#define MAX_TEXTLINES_HISTORY 1000

typedef struct
{
    int printx, printy;
    int scrollpos;
    int lines;
    GList *linelist;
    char *curline;
    char *linepos;
}
GUI_WINDOW_REC;

typedef void GUI_CHAN_REC;
typedef void GUI_DCC_REC;

#endif

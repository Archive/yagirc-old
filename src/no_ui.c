#include "config.h"

#ifndef USE_GUI
#ifndef USE_TEXTUI
#ifndef USE_SCRIPT
#error You need to have USE_SCRIPT enabled from Makefile to make BOT-version of yagIRC...
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <pwd.h>
#include <unistd.h>

#include <glib.h>

#include "irc.h"
#include "commands.h"
#include "script.h"

typedef struct
{
    int handle;
    int mode;
    int tag;
    GUI_INPUT_FUNC func;
    void *data;
}
INPUT_REC;

typedef struct
{
    int ms;
    int gone;
    int tag;
    GUI_TIMEOUT_FUNC func;
    void *data;
}
TIMEOUT_REC;

static GSList *inputlist, *timelist;
static int maxhandle, mintimeout, incount, timecount;

static int quit_program;

char *default_server;
int default_server_port;
char *default_nick;
char *real_name;
char *user_name;

static int verbose;

/* Create DCC transfer window */
void gui_dcc_init(DCC_REC *dcc) {}
/* Clear all text from window */
void gui_window_clear(WINDOW_REC *win) {}
/* Update DCC transfer window */
void gui_dcc_update(DCC_REC *dcc) {}
/* Close DCC transfer window */
void gui_dcc_close(DCC_REC *dcc) { g_free(dcc); }
/* Destroy DCC transfer window */
void gui_dcc_force_close(DCC_REC *dcc) {}

/* DCC chat connection established - do something.. */
void gui_dcc_chat_init(DCC_REC *dcc) {}
/* Text received from DCC chat - write to screen */
void gui_dcc_chat_write(DCC_REC *dcc, char *str) {}
/* Hilight channel button */
void gui_channel_hilight(CHAN_REC *chan) {}
/* Dehilight channel button */
void gui_channel_dehilight(CHAN_REC *chan) {}
/* Select channel in specific window, if channel is in some other window,
   move it from there */
void gui_select_channel(WINDOW_REC *win, CHAN_REC *chan) {}
/* Redraw nick list */
void gui_nicklist_redraw(WINDOW_REC *window) {}

/* Change nick name */
void gui_nick_change(SERVER_REC *server, char *from, char *to) {}
/* Change nick's mode (@, +) */
void gui_change_nick_mode(CHAN_REC *chan, char *nick, char type) {}
/* Someone joined some channel.. If nick==NULL, you joined */
void gui_channel_join(CHAN_REC *chan, char *nick) {}
/* Someone left some channel.. If nick==NULL, you left, if chan==NULL someone
   quit from IRC */
void gui_channel_part(CHAN_REC *chan, char *nick) {}

/* Toggle window autoraising */
void gui_set_autoraise(WINDOW_REC *win, int on) {}

/* Change topic of channel */
void gui_change_topic(CHAN_REC *chan) {}

/* Display channel modes dialog */
void gui_channel_modes(CHAN_REC *chan) {}

void printtext(char *str, WINDOW_REC *win)
{
    if (verbose)
        printf("%s\n", str);
}

/* Initialize IRC window */
void gui_window_init(WINDOW_REC *win, WINDOW_REC *parent)
{
    win->drawfunc = (DRAW_FUNC) printtext;
    win->drawfuncdata = win;
}

/* Deinitialize IRC window */
void gui_window_deinit(WINDOW_REC *win) {}

/* IRC channel/window changed, update everything you can think of.. */
void gui_window_update(WINDOW_REC *win) {}

/* Change focus to specified window */
void gui_window_select(WINDOW_REC *win) {}

/* Change to any new window in same top window */
void gui_window_select_new(WINDOW_REC *win) {}

/* Quit program requested */
void gui_exit(void)
{
    quit_program = 1;
}

/* Inform GUI what connection state we are in - 0 = disconnected, 1 = socket
   connected, 2 = IRC server ready */
void gui_connected(SERVER_REC *serv, int state) {}

/* Someone in notify list joined IRC */
void gui_notify_join(char *nick) {}
/* Someone in notify list left IRC */
void gui_notify_part(char *nick) {}

/* Update status bar */
void gui_update_statusbar(WINDOW_REC *win) {}

/* Set away state */
void gui_set_away(int away) {}

/* Create dialog to ask password */
void gui_ask_password(char *label, GUI_PASSWORD_FUNC func, void *data) {}

/* Change channel button label */
void gui_channel_change_name(CHAN_REC *chan, char *name) {}

int gui_input_add(int fh, int mode, GUI_INPUT_FUNC func, void *data)
{
    INPUT_REC *inp;

    inp = g_new(INPUT_REC, 1);
    inp->handle = fh;
    inp->mode = mode;
    inp->func = func;
    inp->data = data;
    inp->tag = ++incount;
    if (fh > maxhandle) maxhandle = fh;

    inputlist = g_slist_append(inputlist, inp);
    return incount;
}

void gui_input_remove(int tag)
{
    GSList *tmp, *rec;
    int maxhandle;

    rec = NULL; maxhandle = -1;
    for (tmp = inputlist; tmp != NULL; tmp = tmp->next)
    {
        INPUT_REC *inp;

        inp = (INPUT_REC *) tmp->data;
        if (inp->tag == tag)
        {
            g_free(tmp->data);
            rec = tmp;
        }
        else
        {
            if (inp->handle > maxhandle) maxhandle = inp->handle;
        }
    }

    if (rec != NULL)
    {
        incount--;
        g_slist_remove_link(inputlist, rec);
    }
}

int gui_timeout_new(int ms, GUI_TIMEOUT_FUNC func, void *data)
{
    TIMEOUT_REC *t;

    t = g_new(TIMEOUT_REC, 1);
    t->ms = ms;
    t->gone = 0;
    t->func = func;
    t->data = data;
    t->tag = ++timecount;
    if (ms < mintimeout) mintimeout = ms;
    timelist = g_slist_append(timelist, t);
    return timecount;
}

void gui_timeout_remove(int tag)
{
    GSList *tmp, *rec;

    rec = NULL; mintimeout = 1000;
    for (tmp = timelist; tmp != NULL; tmp = tmp->next)
    {
        TIMEOUT_REC *t;

        t = (TIMEOUT_REC *) tmp->data;
        if (t->tag == tag)
        {
            g_free(tmp->data);
            rec = tmp;
        }
        else
        {
            if (t->ms < mintimeout) mintimeout = t->ms;
        }
    }
    if (rec != NULL)
    {
        timecount--;
        g_slist_remove_link(timelist, rec);
    }
}

void gui_init(void)
{
    inputlist = timelist = NULL;
    maxhandle = -1; mintimeout = 1000; incount = 0; timecount = 0;
    quit_program = 0;
}

void gui_deinit(void)
{
    g_slist_foreach(inputlist, (GFunc) g_free, NULL);
    g_slist_free(inputlist);

    g_slist_foreach(timelist, (GFunc) g_free, NULL);
    g_slist_free(timelist);

    g_free(user_name);
    g_free(real_name);
    g_free(default_nick);
    if (default_server != NULL) g_free(default_server);
}

void main_loop(void)
{
    GSList *tmp;
    static fd_set infd, outfd;

    while (!quit_program)
    {
        struct timeval tv;

        FD_ZERO(&infd); FD_ZERO(&outfd);
        for (tmp = inputlist; tmp != NULL; tmp = tmp->next)
        {
            INPUT_REC *inp;

            inp = (INPUT_REC *) tmp->data;

            if (inp->mode == GUI_INPUT_READ)
                FD_SET(inp->handle, &infd);
            else
                FD_SET(inp->handle, &outfd);
        }

        tv.tv_sec = mintimeout/1000;
        tv.tv_usec = (mintimeout%1000)*1000;
        if (select(maxhandle+1, &infd, &outfd, NULL, &tv) > 0)
        {
            if (quit_program) break;
            for (tmp = inputlist; tmp != NULL; tmp = tmp->next)
            {
                INPUT_REC *inp;

                inp = (INPUT_REC *) tmp->data;
                if ((inp->mode == GUI_INPUT_READ && FD_ISSET(inp->handle, &infd)) ||
                    (inp->mode == GUI_INPUT_WRITE && FD_ISSET(inp->handle, &outfd)))
                {
                    inp->func(inp->data);
                    if (quit_program) break;
                }
            }
        }
        if (quit_program) break;

        for (tmp = timelist; tmp != NULL; tmp = tmp->next)
        {
            TIMEOUT_REC *t;

            t = (TIMEOUT_REC *) tmp->data;
            t->gone += mintimeout;
            if (t->ms >= t->gone)
            {
                t->gone = 0;
                t->func(t->data);
                if (quit_program) break;
            }
        }
    }
}

void gui_setup(void)
{
    struct passwd *entry;
    char *ptr;

    /* get user information */
    user_name = getenv("IRCUSER");
    real_name = getenv("IRCNAME");
    default_server = getenv("IRCSERVER");

    default_server_port = 6667;
    if (default_server != NULL)
    {
        /* get server port */
        ptr = strchr(default_server, ':');
        if (ptr != NULL)
        {
            *ptr = '\0';
            if (sscanf(ptr, "%d", &default_server_port) != 1)
                default_server_port = 6667;
        }
    }

    /* get passwd-entry */
    entry = getpwuid(getuid());
    if (entry != NULL)
    {
        user_name = entry->pw_name;
        if (real_name == NULL) real_name = entry->pw_gecos;
    }

    ptr = getenv("IRCNICK");
    if (ptr == NULL)
    {
        if (user_name != NULL)
            ptr = user_name;
        else
            ptr = "someone";
    }

    default_nick = ptr;
    if (user_name == NULL || *user_name == '\0') user_name = default_nick;
    if (real_name == NULL || *real_name == '\0') real_name = user_name;

    user_name = g_strdup(user_name);
    real_name = g_strdup(real_name);
    default_nick = g_strdup(default_nick);
    if (default_server != NULL) default_server = g_strdup(default_server);
}

int main (int argc, char *argv[])
{
    WINDOW_REC *win;
    int pid;

    verbose = argc == 2 && strcmp(argv[1], "-v") == 0;

    pid = fork();
    if (pid == -1)
    {
        g_error("fork() failed.\n");
        return 1;
    }

    if (pid != 0)
        g_print("forking to background process %d\n", pid);
    else
    {
        gui_setup();
        gui_init();
        irc_init();

        win = irc_window_new(NULL, NULL);

        script_load("bot.pl");

        main_loop();
        irc_window_close(win);

        irc_deinit();
        gui_deinit();
    }

    return 0;
}
#endif
#endif

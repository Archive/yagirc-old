#include "config.h"

/* Dummy header for libintl.h */

#ifndef USE_GNOME
	#ifdef ENABLE_NLS
		#undef __OPTIMIZE__
		#include <libintl.h>
		#define _(String) gettext((String))
	#else
		#define _(String) (String)
	#endif
#endif
#define N_(String) (String)

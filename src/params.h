#include "config.h"

#ifndef __PARAMS_H
#define __PARAMS_H

enum
{
    RET_ERR_PARAM = -1, /* g_return_val_if_fail() failed */
    RET_ERROR = 0, /* just some error, reason already printed to screen */
    RET_OK = 1, /* function worked just fine */
    RET_NOT_ENOUGH_PARAMS, /* not enough parameters given */
    RET_NOT_CONNECTED, /* not connected to IRC server */
    RET_NOT_JOINED, /* not joined to any channels in this window */
    RET_ERR_GETSOCKNAME, /* getsockname() failed */
    RET_ERR_LISTEN, /* listen() failed */
    RET_MULTIPLE_MATCHES, /* multiple matches found, didn't do anything */
    RET_NICK_NOT_FOUND, /* nick not found */
    RET_CHAN_NOT_FOUND, /* channel not found */
    RET_SERVER_NOT_FOUND, /* server not found */
};

extern char *ret_texts[];

#endif

#include "config.h"

#ifndef __FLOOD_H
#define __FLOOD_H

/* Initialize flood protection */
void init_flood(SERVER_REC *server);

/* Deinitialize flood protection */
void deinit_flood(SERVER_REC *server);

/* All messages should go through here.. */
void flood_newmsg(SERVER_REC *server, int type, char *nick);

#endif

/* src/config.h.in.  Generated automatically from configure.in by autoheader. */

#undef PACKAGE

#undef VERSION

#undef HAVE_LIBSM

#undef HAVE_SOCKS_H

#undef USE_GUI

#undef USE_SCRIPT

#undef USE_GNOME

#undef USE_SOCKS5

#undef USE_SOCKS4

#undef USE_IMLIB

#undef USE_CRYPT

#undef USE_ESD

#undef PREFIX

#undef EXEC_PREFIX

#undef SCRIPT_PREFIX

#undef USE_PTHREADS

#undef USE_TEXTUI

#undef UNIX

#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY
#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_GETTEXT

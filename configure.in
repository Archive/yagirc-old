dnl Process this file with autoconf to produce a configure script.

AC_INIT()

AM_INIT_AUTOMAKE(yagirc, 0.65.7)
AM_CONFIG_HEADER(src/config.h)

dnl CFLAGS=""

dnl Checks for programs.
dnl ===================
AC_PROG_CC
AM_PROG_LIBTOOL
AC_PROG_INSTALL

dnl Define DEBUG to get debugging output, -pg for profiling.
dnl CFLAGS="$CFLAGS -DDEBUG"
dnl Some extra warnings
dnl CFLAGS="$CFLAGS -Wtraditional -Wshadow -Wmissing-prototypes"

ALL_LINGUAS="pt_BR ko"
AM_GNU_GETTEXT

dnl Only use -Wall if we have gcc
if test "x$GCC" = "xyes"; then
  if test -z "`echo "$CFLAGS" | grep "\-Wall" 2> /dev/null`" ; then
    CFLAGS="$CFLAGS -Wall"
  fi
fi


AC_DEFINE(UNIX)

################################################################
# yag need to be linked with libresolv and libnss_dns
# if not after after pthread_create in net_connect,
# I sometime  some error/crash on my debian 2 SMP.
#
# don't ask me why.

    AC_CHECK_LIB(resolv, dn_expand, LIBS=" -lresolv $LIBS" ,,)
    AC_CHECK_LIB(nss_dns, main, LIBS=" -lnss_dns $LIBS" ,,)

#################################################################

#
# gui/textui
#

AC_ARG_ENABLE(textui,
[  --with-textui                  enable textui ])

if test "x$with_textui" != "x"; then
AC_CHECK_LIB(ncurses, initscr, LIBS=" -lncurses $LIBS" ,,)
AC_DEFINE(USE_TEXTUI)
mode_textui=yes
else
AC_DEFINE(USE_GUI)
mode_textui=no
fi

#
# SOCKS5 check
#

AC_MSG_CHECKING(whether to support SOCKS)
AC_ARG_ENABLE(socks,
[  --disable-socks            Compile with SOCKS 4 or 5.],
 socks=$enableval, socks=yes)

if test "$socks" = yes; then
    AC_CHECK_LIB(socks5, SOCKSconnect, AC_DEFINE(USE_SOCKS5) LIBS=" -lsocks5 $LIBS" ,,)
    AC_CHECK_LIB(socks, Rconnect, AC_DEFINE(USE_SOCKS4)  LIBS="-lsocks $LIBS" ,,)
    AC_CHECK_HEADERS(socks.h)
fi

#
# Gtk 1.0.3 and Gtk 1.1.0 check
#

dnl GTK 1.0.2 is needed for g_strdown() & GPOINTER_TO_INT macros
AM_PATH_GTK(1.0.3,
  	    [LIBS="$LIBS $GTK_LIBS"
            CFLAGS="$CFLAGS $GTK_CFLAGS"],
            AC_MSG_ERROR(Cannot find GTK: Is gtk-config in path?))

#
# Gnome check
#

if test "$mode_textui" = no; then
gnome=yes
AC_ARG_ENABLE(gnome,
[  --disable-gnome                  disable use of gnome],
        gnome=$enableval, gnome=yes)
fi

if test "$gnome" = yes; then
GNOME_INIT
AC_DEFINE(USE_GNOME)
fi

#
# Pthread check
#

AC_ARG_ENABLE(pthread,
[  --disable-pthread            Compile with pthread],
 pthread=$enableval, pthread=yes)

if test "$pthread" = yes; then
AC_CHECK_HEADERS(pthread.h, AC_DEFINE(USE_PTHREADS) )
AC_CHECK_LIB(pthread, pthread_create, LIBS="$LIBS -lpthread" ,,)
fi


#
# XPM check
#
if test "$mode_textui" = no; then
xpm=yes
AC_ARG_ENABLE(xpm, 
[  --disable-xpm                  disable use of XPM pixmaps through libXpm],
        xpm=$enableval, xpm=yes)
fi

if test "$xpm" = yes; then
    AC_CHECK_LIB(Xpm, XpmCreatePixmapFromData,
        [GFX_LIBS="$GFX_LIBS -lXpm"],,
        )
fi

#
# Imlib check
#
if test "$mode_textui" = no; then
imlib=yes
AC_ARG_ENABLE(imlib,
[  --disable-imlib                  disable use of imlib ],
        imlib=$enableval, imlib=yes)
fi

if test "$imlib" = yes; then
AC_DEFINE(USE_IMLIB)
	AC_CHECK_LIB(z, gzread, GFX_LIBS="-lz $GFX_LIBS",,)
        AC_CHECK_LIB(png, png_get_valid, GFX_LIBS="-lpng $GFX_LIBS",, -lz)
	AC_CHECK_LIB(jpeg, jpeg_destroy_compress, GFX_LIBS="-ljpeg $GFX_LIBS",,)
        AC_CHECK_LIB(tiff, TIFFGetVersion, GFX_LIBS="-ltiff $GFX_LIBS", AC_CHECK_LIB(tiff34, TIFFGetVersion, GFX_LIBS="-ltiff34 $GFX_LIBS",, -ljpeg -lz -lm), -ljpeg -lz -lm)
	AC_CHECK_LIB(gif, main, GFX_LIBS="-lgif $GFX_LIBS",,)
        AC_CHECK_LIB(gdk_imlib, gdk_imlib_init, GFX_LIBS="-lgdk_imlib $GFX_LIBS",, $GFX_LIBS)
fi

#
# EsounD check
#

if test "$mode_textui" != yes; then
esd=yes
AC_ARG_ENABLE(esd,
[  --disable-esd                  disable use of esd ],
        esd=$enableval, esd=yes)
fi

if test "$esd" = yes; then
AC_CHECK_LIB(esd,main, GFX_LIBS="-lesd $GFX_LIBS",,)
AC_DEFINE(USE_ESD)
fi

#
# script check (do this late, since we are going to add a ton
# of libraries/cflags)
#

script=yes
AC_ARG_ENABLE(script,
[  --disable-script                  disable use of script],
        script=$enableval, script=yes)

if test "$script" = yes; then
# Checks for perl
AC_PATH_PROG(perlpath, perl)
AC_MSG_CHECKING(for Perl compilation flags)
PERL_CFLAGS=`$perlpath -MExtUtils::Embed -e ccopts 2>/dev/null`
PERL_LDFLAGS=`$perlpath -MExtUtils::Embed -e ldopts 2>/dev/null`
if test "_$PERL_CFLAGS" = _ ; then
  AC_MSG_RESULT([not found, building without script support.])
else
#    echo USE_SCRIPT=1 >> src/yconfig.h
#    echo perl_path=$SCRIPT_PATH >> src/yconfig.h
  AC_MSG_RESULT(ok)
  CPPFLAGS=" $CPPFLAGS $PERL_CFLAGS"
  LIBS="$LIBS $PERL_LDFLAGS"
  AC_DEFINE(USE_SCRIPT)
fi
fi

#
# Yagirc install path
#

dnl AC_PREFIX_PROGRAM(yagirc)

if test "${prefix}" = "NONE"; then
        prefix="/usr/local"
fi
        AC_DEFINE_UNQUOTED(PREFIX, "$prefix")
        AC_DEFINE_UNQUOTED(SCRIPT_PREFIX, "$prefix/share/yagirc/")


if test "${exec_prefix}" = "NONE"; then
        exec_prefix=$prefix
fi
        AC_DEFINE_UNQUOTED(EXEC_PREFIX, "$exec_prefix")

AC_SUBST(GFX_LIBS)
AC_SUBST(LIBS)
AC_SUBST(PTHREAD_LIB)
LIBS="$LIBS $GFX_LIBS $GNOME_LIBS $GNOME_LIBDIR $PTHREAD_LIB $GNOMEUI_LIBS"
AC_OUTPUT([
Makefile
pixmaps/Makefile
po/Makefile.in
intl/Makefile
src/Makefile
],[sed -e "/POTFILES =/r po/POTFILES" po/Makefile.in > po/Makefile])


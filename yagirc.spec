# Note that this is NOT a relocatable package
%define ver      0.66.0
%define rel      1
%define prefix   /usr/local

Summary: yagIRC -- Yet Another GTK+ IRC Client
Name: yagIRC
Version: %ver
Release: %rel
Copyright: GPL
Group: X11/Applications
Source: http://yagirc.home.ml.org/yagirc-%{ver}.tar.gz
BuildRoot: /tmp/yagirc-root
Packager: Marcus Brubaker <spoon@elpaso.net>
URL: http://yagirc.home.ml.org/
Requires: imlib >= 1.6
Requires: glib >= 1.0.3
Requires: gtk+ >= 1.0.3
Requires: gnome-libs = 0.20
Requires: perl >= 5.004
Docdir: %{prefix}/doc

%description
yagIRC is "Yet Another GTK+ based IRC client".

%prep
%setup -n yagirc-0.66.0

%build
./configure --prefix=%{prefix}
make

%install
rm -rf $RPM_BUILD_ROOT

./configure --prefix=$RPM_BUILD_ROOT%{prefix}
make install

%clean
rm -rf $RPM_BUILD_ROOT

%post

%postun

%files
%defattr(-, root, root)

%{prefix}/bin/*
%{prefix}/etc/*
%{prefix}/share/yagirc/*
%{prefix}/share/yagirc/pixmaps/*

%doc COPYRIGHT
%doc ChangeLog
%doc README
%doc TODO
